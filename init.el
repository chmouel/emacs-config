;; Copyright Chmouel Boudjnah <chmouel@chmouel.com>
;; Emacs init
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; Startup time
(defun chmou-display-startup-time ()
  (message
   "Emacs loaded in %s with %d garbage collections."
   (emacs-init-time)
   gcs-done))

(add-hook 'emacs-startup-hook #'chmou-display-startup-time)

(defun my-short-hostname()
  (string-match "[0-9A-Za-z-]+" (system-name))
  (downcase (substring system-name (match-beginning 0) (match-end 0))))


;;----------------------------------------------------------------------------
;; Bootstrap config
;;----------------------------------------------------------------------------
(setq custom-file (expand-file-name "auto-save-list/custom.el" user-emacs-directory))

(require 'package)
(add-to-list 'package-archives
         '("melpa" . "https://melpa.org/packages/") t)
(setq use-package-always-defer t
      use-package-always-ensure t)

;; Init Early local
(require 'init-local nil t)

(require 'init-functions)
(require 'init-keybindings)

;; Requires
(require 'init-theme)
(require 'init-emacs)
(require 'init-abbrev)
(require 'init-popmark)
(require 'init-dired)
(require 'init-desktop)
(require 'init-project)

;; ;; Packages
(require 'init-packages)
(require 'init-completion)
(require 'init-eshell)
(require 'init-magit)
(require 'init-evil)

;; ;; Programming modes
(require 'init-programming)
(require 'init-yasnippet)
(require 'init-elisp)
(require 'init-python)
(require 'init-yaml)
(require 'init-org)
(require 'init-sh)
(require 'init-makefile)
(require 'init-javascript)
(require 'init-markdown)
(require 'init-go)
(require 'init-rust)
(require 'init-web)
(require 'init-centaur-tabs)
(require 'init-copilot)
(require 'init-eglot)
(require 'init-treesitter)

(require 'init-llm)
