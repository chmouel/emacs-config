;; Projects
(use-package consult-project-extra
  :bind ("C-c f" . consult-project-extra-find))

(use-package project
  :ensure t
  :bind
  ("C-c b" . (lambda ()(interactive)
               (let ((consult-buffer-filter))
                 (add-to-list 'consult-buffer-filter "\\*")
                 (call-interactively 'consult-project-buffer))))
  ("C-x p o" . (lambda ()
                 (interactive)
                 (let ((project-switch-commands 'consult-project-extra-find))
                   (call-interactively 'project-switch-project))))
  ("C-x p p" . my-switch-to-project-dired)
  ("C-x p r" . my-find-recent-file-in-project)
  ("C-x p h" . project-query-replace-regexp)
  ("C-x p P" . project-switch-project)
  ("C-c h" . project-find-regexp)
  (:map project-prefix-map
        ("v"  . vterm-toggle)
        ("m"  . magit-project-status))
  (:map isearch-mode-map
        ("C-f" . my-project-from-isearch))
  :custom
  (project-mode-line t)
  (project-switch-commands
   '((project-find-file "Find file")
     (project-find-regexp "Find regexp")
     (project-find-dir "Find directory")
     (magit-project-status "Magit")
     (vterm-toggle "Vterm")))
  (project-list-file (locate-user-emacs-file "auto-save-list/project-list.file")))

(defun my-project-find-file-in-dir (dir)
  "`project-find-file' but uses the given dir instead of the current."
  (interactive "P")
  (let* ((pr (project--find-in-directory dir))
         (root (project-root pr))
         (dirs (list root)))
    (project-find-file-in
     (or (thing-at-point 'filename)
         (and buffer-file-name (file-relative-name buffer-file-name root)))
     dirs pr nil)))

(defun my-project-from-isearch ()
  "Invoke `project-find-regexp' from isearch."
  (interactive)
  (let ((query (if isearch-regexp
		           isearch-string
		         (regexp-quote isearch-string))))
    (isearch-update-ring isearch-string isearch-regexp)
    (let (search-nonincremental-instead)
      (ignore-errors (isearch-done t t)))
    (project-find-regexp query)))

;;; Functions
(defun my-switch-to-project-dired ()
  (interactive)
  (let ((project-switch-commands 'project-dired))
    (call-interactively 'project-switch-project)))

(defun my-switch-to-project-search ()
  (interactive)
  (let ((project-switch-commands 'project-search))
    (call-interactively 'project-switch-project)))

(defun my-find-recent-file-in-project(project)
  "Find a recent file in the PROJECT."
  (interactive
   (list
    (project-root (project-current t))))
  (let ((file
         (consult--read
          recentf-list
          :predicate (lambda (x) (string-match-p project x 0))
          :history 'file-name-history
          :category 'recentf
          :sort nil
          :prompt "Recentf: ")))
    (find-file file)))

(provide 'init-project)
