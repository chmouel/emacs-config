(use-package editorconfig)

(defun my-copilot-chat-jump-to-buffer ()
  "Switch to Copilot Chat buffer, side by side with the current code editing buffer."
  (interactive)
  (require 'copilot-chat)
  (unless (copilot-chat--ready-p)
    (copilot-chat-reset))
  (pop-to-buffer (copilot-chat--get-buffer)))

(use-package copilot
  :hook
  (prog-mode . copilot-mode)
  (log-edit-mode . copilot-mode)
  (git-commit-setup-hook . copilot-mode)
  (vc-git-log-edit-mode . copilot-mode)
  :general
  (general-leader "lc" '(copilot-mode :wk "Toggle Copilot Mode"))
  :bind
  (:map copilot-completion-map
        ("C-g" . copilot-clear-overlay)
        ("C-j" . copilot-next-completion)
        ("C-k" . copilot-previous-completion)
        ("C-f" . copilot-accept-completion)
        ("C-l" . copilot-panel-complete))
  :custom
  (copilot-idle-delay 0.2)
  (copilot-max-char -1)
  (copilot-indent-offset-warning-disable t)
  :commands
  (copilot-mode))

(setq copilot-chat-commit-prompt "Here is the result of running `git diff
--cached`. Based on this, suggest a **Conventional Commit message**. Ensure the
message includes both a clear title describing the change and make sure a body
explaining the change, do not invent anything new just try to comprehend the
diff and explain it.

- Do not add extra markdown formatting.
- Always make sure the commit message is in markdown format.
- Do not include any additional text outside the commit message
- Make sure the title is a max of 50 character long and not more. 
- The summaries needs to be wrapped to 80 character long and not more (or break
  the line).

# Conventional Commits 1.0.0

## Summary

Conventional Commits is a specification for commit messages that follows
these rules to ensure clarity and consistency:

### Format
<type>[optional scope]: <description>

[body]

### Types
1. fix: A bug fix correlating to a PATCH version.
2. feat: A new feature correlating to a MINOR version.

Other types include:  
- build: Changes to build systems or dependencies.  
- chore: Maintenance tasks (e.g., dependency updates).  
- ci: Changes to CI configuration.  
- refactor: Code changes not adding features or fixing bugs.  
- test: Changes to or addition of tests.  

Here is the result of `git diff --cached`:")

(use-package copilot-chat
  :custom
  (copilot-chat-frontend 'markdown)
  (copilot-chat-model "claude-3.7-sonnet")
  :init
  (setopt request-storage-directory (locate-user-emacs-file "auto-save-list/request")
          copilot-install-dir (locate-user-emacs-file "auto-save-list/copilot"))
  :bind
  (:map copilot-chat-prompt-mode-map
        ("C-M-w" . my-copilot-chat-copy-source-block)
        ("C-<return>" . copilot-chat-prompt-send)
        ("C-q" . delete-window))
  :config
  (setq copilot-chat-prompts copilot-chat-markdown-prompt)
  (defun my-copilot-chat-copy-source-block ()
    "Copy the source block at point to kill ring."
    (interactive)
    (let* ((temp-buffer-name "*copilot-kr-temp*"))
      (with-current-buffer (get-buffer-create temp-buffer-name)
        (erase-buffer)
        (copilot-chat-yank)
        (kill-ring-save (point-min) (point-max))
        (kill-buffer))
      (message "Source block copied to kill ring")))

  (defun copilot-chat-prompt-transient-menu ()
    "Show a transient menu for Copilot Chat actions."
    (interactive)
    (unless (use-region-p)
      (mark-defun))
    (transient-define-prefix copilot-chat-prompt-menu ()
      "Copilot Chat Menu"
      ["Copilot Chat Actions"
       ["Target"
        ("c" "Commit" copilot-chat-insert-commit-message)
        ("o" "Optimize" copilot-chat-optimize)
        ("r" "Review" copilot-chat-review)
        ("f" "Fix" copilot-chat-fix)
        ("e" "Explain" copilot-chat-explain)
        ("d" "Doc" copilot-chat-doc)]
       ["Commands"
        ("d" "Display chat" copilot-chat-display)
        ("h" "Hide chat" copilot-chat-hide)
        ("R" "Reset & reopen" (lambda ()
                                (interactive)
                                (copilot-chat-reset)
                                (copilot-chat-display)))
        ("x" "Reset" copilot-chat-reset)
        ("g" "Go to buffer" copilot-chat-switch-to-buffer)
        ("m" "Set model" copilot-chat-set-model)
        ("q" "Quit" transient-quit-one)]
       ["Actions"
        ("p" "Custom prompt" copilot-chat-custom-prompt-selection)
        ("i" "Ask and insert" copilot-chat-ask-and-insert)
        ("m" "Insert commit message" copilot-chat-insert-commit-message)
        ("b" "Buffers" copilot-chat-transient-buffers)]
       ["Data"
        ("y" "Yank last code block" copilot-chat-yank)
        ("s" "Send code to buffer" copilot-chat-send-to-buffer)]])
    (copilot-chat-prompt-menu))
  :general
  (general-leader
    '(normal visual)
    "a" '(:ignore t :wk "Copilot Chat")
    "as" '(my-llm-save-file :wk "Save Copilot Chat")
    "aS" '(copilot-chat-custom-prompt-selection :wk "Prompt Selection Copilot Chat")
    "ac" '(copilot-chat-insert-commit-message :wk "Insert Commit Message")
    "ap" '(copilot-chat-prompt-transient-menu :wk "Copilot Chat Prompt Menu")
    "am" '(copilot-chat-set-model :wk "Copilot set model")
    "ay" '(copilot-chat-yank :wk "Yank Copilot Chat")
    "aw" '(my-copilot-chat-copy-source-block :wk "Copy Kill Ring Copilot Chat")
    "aY" '(copilot-chat-yank-pop :wk "Pop Yank Copilot Chat")
    "ab" '(copilot-chat-display :wk "Toggle Copilot Chat")
    "aa" '(my-copilot-chat-jump-to-buffer :wk "Switch to Copilot Chat buffer")
    "aq" '(copilot-chat-custom-prompt-selection :wk "Custom Prompt")
    "af" '(copilot-chat-custom-prompt-function :wk "Ask about current function"))
  :commands
  (copilot-chat-mode))

(provide 'init-copilot)
