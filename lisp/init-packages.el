(use-package password-store)

(use-package devdocs
  :bind
  (:map devdocs-mode-map
        ("C-u" . evil-scroll-up)
        ("C-d" . evil-scroll-down)
        ("j" . evil-next-line)
        ("k" . evil-previous-line))
  :custom
  (devdocs-data-dir (expand-file-name "~/.cache/devdocs")))

(use-package gcmh
  :demand
  :config (gcmh-mode 1))

(use-package hydra
  :config
  (global-set-key (kbd "C-x t") 'hydra-tab-bar/body)
  (defhydra hydra-tab-bar (:hint nil)
    "
^Tab Operations^                   ^Navigation^                 ^Utilities^
---------------------------------------------------------------------------------------
_f_: find-file-other-tab           _o_: Next tab               _G_: tab-group
_RET_: tab-switch                  _O_: Previous tab           _M_: tab-move-to
_r_: find recent file              _1_: Close other tabs       _N_: tab-new-to
_x_: tab-close                     _d_: dired-other-tab        _p_: project-other-tab
_1_: tab-close-other               _b_: switch-to-buffer       _u_: tab-undo
_2_: tab-new                       _t_: other-tab-prefix       _m_: tab-move
_n_: tab-duplicate                 _^ f_: tab-detach
_q_: Quit
"
    ;; Tab Operations
    ("f" find-file-other-tab :color blue)
    ("RET" tab-switch :color blue)
    ("r" (lambda () (interactive) (tab-new) (consult-recent-file)) :color blue) ;; Lambda for recent files in new tab
    ("x" tab-close :color blue)
    ("1" tab-close-other :color blue)
    ("2" tab-new :color blue)
    ("d" dired-other-tab :color blue)
    ("b" switch-to-buffer-other-tab :color blue)
    ("t" other-tab-prefix :color blue)
    ("m" tab-move :color blue)
    ("n" tab-duplicate :color blue)
    ("^ f" tab-detach :color blue)

    ;; Navigation
    ("o" tab-next :color red)
    ("O" tab-previous :color red)

    ;; Utilities
    ("G" tab-group :color blue)
    ("M" tab-move-to :color blue)
    ("N" tab-new-to :color blue)
    ("p" project-other-tab-command :color blue)
    ("u" tab-undo :color blue)

    ;; Quit
    ("q" nil "quit" :color blue)))

(use-package no-littering)

(use-package easy-hugo
  :custom
  (easy-hugo-postdir "content/posts")
  (easy-hugo-server-flags "-D")
  (easy-hugo-url "https://blog.chmouel.com")
  (easy-hugo-basedir (expand-file-name (concat (getenv "GOPATH") "/src/github.com/chmouel/blog/"))))

(use-package emacs
  :bind ("C-x b" . consult-buffer))

;;; Browse Kill Ring
(use-package browse-kill-ring
  :custom (browse-kill-ring-show-preview nil)
  :bind (("C-c k" . browse-kill-ring)))

(use-package avy
  :general
  (general-leader go-ts-mode-map :states 'normal "ly" 'my-copy-function-name-with-ts)
  (general-leader '(visual) "r"  #'my-avy-copy-symbol-paste-at-point)
  :bind
  ([remap goto-char]. avy-goto-char)
  ("M-g j" . evil-avy-goto-word-1)
  ("M-g r" . my-avy-copy-symbol)
  (:map isearch-mode-map ("C-j" . avy-isearch))
  :config
  (defun my-avy-just-copy-line (arg)
    (interactive "p")
    (let ((initial-window (selected-window)))
      (avy-with
          my-avy-just-copy-line
        (let* ((start (avy--line))
               (str
                (buffer-substring-no-properties
                 start
                 (save-excursion
                   (goto-char start)
                   (move-end-of-line arg)
                   (point)))))
          (select-window initial-window)
          (kill-new (concat str "\n"))))))

  (defun my-avy-copy-symbol-paste-at-point (arg)
    (interactive "p")
    (save-excursion
      (call-interactively 'avy-goto-symbol-1)
      (let ((symbol (thing-at-point 'symbol)))
        (when symbol
          (kill-new symbol)
          (message "\"%s\" has been copied" symbol)
          (insert symbol)))))

  (defun my-avy-copy-symbol (arg)
    (interactive "p")
    (save-excursion
      (call-interactively 'avy-goto-symbol-1)
      (let ((symbol (thing-at-point 'symbol)))
        (when symbol
          (kill-new symbol)
          (message "\"%s\" has been copied" symbol))))))

(use-package multiple-cursors
  :custom (mc/list-file (locate-user-emacs-file "auto-save-list/multi-curor-list.el")))

(use-package ace-window
  :bind
  ([remap other-window] . ace-window)
  ("C-j" . ace-window)
  :custom-face
  (aw-leading-char-face
   ((t (:foreground "black" :background "yellow" :weight bold :height 3.0))))
  :custom
  (aw-char-position 'left)
  (aw-keys '(?j ?k ?l ?a ?d)))

(use-package popper
  :disabled
  :bind (("C-0"   . popper-toggle)
         ("M-0"   . popper-cycle)
         ("C-M-0" . popper-toggle-type))
  :custom
  (popper-reference-buffers
   '("\\*Messages\\*"
     "\\*Go Test\\*"
     ;; ".*Copilot-chat.*"
     "\\*eldoc\\*"
     "Output\\*$"
     "\\*Async Shell Command\\*"
     help-mode
     compilation-mode))
  :hook
  (after-init . (lambda ()
                  (interactive)
                  (popper-mode 1)
                  (popper-echo-mode 1)
                  (setq popper-group-function #'popper-group-by-project))))

(use-package undo-fu-session
  :custom
  (undo-fu-session-directory (locate-user-emacs-file "auto-save-list/undo-fu-session"))
  :hook
  (after-init . undo-fu-session-global-mode))

(use-package easy-kill
  :config
  ;; M-w ^ for begin and M-w $ for end of line
  (defun easy-kill-on-path (_n)
    (easy-kill-adjust-candidate 'path (file-name-directory (buffer-file-name))))
  (add-to-list 'easy-kill-alist '(?P path "\n\n"))
  ;; M-w ^ for begin and M-w $ for end of line
  (defun easy-kill-on-eol (_n)
    (easy-kill-adjust-candidate 'eol (point) (line-end-position)))
  (add-to-list 'easy-kill-alist '(?$ eol "\n\n"))
  (defun easy-kill-on-bol (_n)
    (easy-kill-adjust-candidate 'bol (point) (line-beginning-position)))
  (add-to-list 'easy-kill-alist '(?^ bol "\n\n"))
  (defun easy-kill-on-go-path (_n)
    (pcase (if (s-contains? (concat (getenv "GOPATH") "/src/github.com") (buffer-file-name))
               (directory-file-name
                (file-name-directory
                 (s-replace (concat (getenv "GOPATH") "/src/") "" (buffer-file-name)))))
      (`nil (easy-kill-echo "No `gopath' at point"))
      (name (easy-kill-adjust-candidate 'go-name name))))
  (add-to-list 'easy-kill-alist '(?G go-path "\n\n"))
  :bind
  (("M-r" . easy-mark)
   ([remap kill-ring-save] . easy-kill)))

(use-package comment-dwim-2
  :bind (([remap comment-dwim] . comment-dwim-2)))

(use-package smart-shift
  :config (global-smart-shift-mode))

(use-package zygospore
  :bind
  (("C-1" . zygospore-toggle-delete-other-windows)
   ("C-x 1" . zygospore-toggle-delete-other-windows)))

(use-package google-this
  :custom
  (google-this-wrap-in-quotes t)
  :general (general-leader '(normal visual) "so" #'(google-this :wk "Google at point"))
  :bind (("C-c ?" . google-this)))

(use-package dumb-jump
  :config (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

(use-package drag-stuff
  :config (drag-stuff-global-mode 1)
  :bind (("M-<up>" . drag-stuff-up) ("M-<down>" . drag-stuff-down)))

(use-package crux
  :bind
  (("C-k"             . crux-smart-kill-line)
   ("C-S-k"           . crux-kill-whole-line)
   ("C-l"             . crux-switch-to-previous-buffer)
   ("C-S-l"           . crux-other-window-or-switch-buffer)
   ("M-S-<down>"      . crux-duplicate-current-line-or-region)
   ([remap beginning-of-line] . crux-move-beginning-of-line)
   ("S-<return>"      . crux-smart-open-line)
   ([remap open-line] . crux-smart-open-line-above)
   ("C-x 4 t"         . (lambda (arg)
                          (interactive "p")
                          (crux-transpose-windows arg)
                          (other-window arg)))))

(use-package anzu
  :bind
  (([remap query-replace]        . anzu-query-replace)
   ([remap query-replace-regexp] . anzu-query-replace-regexp)
   ("C-S-h"                      . anzu-query-replace-regexp)
   :map
   isearch-mode-map
   ([remap isearch-query-replace]        . anzu-isearch-query-replace)
   ([remap isearch-query-replace-regexp] . anzu-isearch-query-replace-regexp)
   ("C-h"                                . anzu-isearch-query-replace))
  :custom (anzu-replace-to-string-separator "↝")
  :hook (after-init . global-anzu-mode))

(use-package wgrep-deadgrep
  :bind (:map deadgrep-mode-map ("e" . wgrep-change-to-wgrep-mode))
  :hook (deadgrep-finished . wgrep-deadgrep-setup))

(use-package deadgrep
  :bind
  (:map deadgrep-mode-map ("C-e" . deadgrep-edit-mode)))

(use-package helpful
  :bind
  (([remap describe-function] . helpful-callable)
   ([remap describe-command] . helpful-command)
   ([remap describe-variable] . helpful-variable)
   ([remap describe-key] . helpful-key)
   :map
   emacs-lisp-mode-map
   ("C-c C-d" . helpful-at-point)))

(use-package rg
  :config (evil-set-initial-state 'rg-mode 'emacs)
  :custom (rg-group-result nil)
  :hook
  (rg-mode
   .
   (lambda ()
     (evil-emacs-state)))
  :commands rg-run)

(use-package wgrep
  :custom
  (wgrep-enable-key "e")
  (wgrep-auto-save-buffer t)
  :hook (grep-setup . wgrep-setup))

;; Direnv
(use-package direnv
  :ensure t
  :custom
  (direnv-always-show-summary nil)
  (direnv-show-paths-in-summary nil)
  :hook (after-init . direnv-mode))

(use-package resize-window
  :bind ("C-c ;" . resize-window))

(use-package ibuffer-project
  :hook
  (ibuffer .
           (lambda ()
             (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
             (ibuffer-update nil t))))

(use-package nerd-icons-ibuffer
  :hook (ibuffer-mode . nerd-icons-ibuffer-mode))


(use-package golden-ratio
  :hook (after-init . golden-ratio-mode))

(provide 'init-packages)

;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
