(use-package devil
  :disabled
  :custom
  ;; Make all keys repeatable
  (devil-all-keys-repeatable t)
  
  ;; Define custom key translations
  (devil-translations
   '(("%k z %k" . "M-,")   ;; Translate %k z %k to M-,
     ("%k z z"  . "C-M-")  ;; Translate %k z z to C-M-
     ("%k z"    . "M-")    ;; Translate %k z to M-
     ("%k %k"   . "%k")    ;; Translate %k %k to %k
     ("%k ,"    . "C-")    ;; Translate %k , to C-
     ("%k"      . "C-")))) ;; Translate %k to C-

(use-package evil
  :commands (evil-local-mode)
  :hook
  (after-init . evil-mode)
  (with-editor-mode . evil-normal-state)
  (org-capture-mode . evil-insert-state)
  (view-mode . evil-motion-state)
  :bind
  ("C-S-z" . evil-mode)
  ("C-<up>"           . (lambda ()(interactive)(evil-line-move -4)(recenter)))
  ("C-<down>"         . (lambda ()(interactive)(evil-line-move 4)(recenter)))
  :config
  (define-key evil-motion-state-map (kbd "RET") nil)
  (define-key evil-inner-text-objects-map "q" 'evil-inner-double-quote)
  (define-key evil-outer-text-objects-map "q" 'evil-a-double-quote)
  (evil-select-search-module 'evil-search-module 'evil-search)
  (dolist (mode '(help-mode
                  helpful-mode
                  package-menu-mode
                  rg-mode
                  eshell-mode
                  xref--xref-buffer-mode
                  flycheck-error-list-mode
                  flymake-project-diagnostics-mode
                  flymake-diagnostics-buffer-mode
                  compilation-mode
                  grep-mode
                  dired-mode
                  ibuffer-mode
                  treemacs-mode
                  go-test-mode
                  Info-mode
                  dashboard-mode
                  gotest-mode
                  deadgrep-mode
                  inferior-emacs-lisp-mode
                  view-mode))
    (evil-set-initial-state mode 'emacs))

  ;; https://github.com/emacs-evil/evil/issues/874#issuecomment-315290644
  (evil-define-text-object my-evil-defun (count &optional beg end type)
    "Select a defun."
    ;; (evil-select-an-object …) for me often results in errors, so reimplement it with
    ;; inner-object instead
    (pcase-let ((`(,beg ,past_end) (evil-select-inner-object 'evil-defun beg end type count)))
      (goto-char past_end)
      (let ((lst-space-addr (re-search-forward "[^[:space:]\n]" nil t)))
        (if lst-space-addr
            `(,beg ,(- lst-space-addr 1))
          `(,beg ,(point-max))))))
  (define-key evil-inner-text-objects-map "f" 'my-evil-defun)
  (define-key evil-outer-text-objects-map "f" 'my-evil-defun)

  (dolist (mode '(devdocs-mode))
    (evil-set-initial-state mode 'motion))

  ;; set c-r as backward search
  (define-key evil-normal-state-map (kbd "C-r") 'evil-ex-search-backward)
  ;; set U as redo
  (define-key evil-normal-state-map (kbd "U") 'undo-redo)

  (defun my-scroll-and-pulse-advice (&rest _)
    (require 'pulse)
    (pulse-momentary-highlight-one-line))

  (dolist (fn '(evil-scroll-down evil-scroll-up))
    (advice-add fn :after #'my-scroll-and-pulse-advice))

  :init
  (setq evil-undo-system                    'undo-redo
        evil-vsplit-window-right            t
        evil-want-integration               t
        evil-want-Y-yank-to-eol             t
        evil-want-keybinding                nil
        evil-want-C-u-scroll                t
        evil-want-Y-yank-to-eol             t
        evil-disable-insert-state-bindings  t
        evil-ex-search-persistent-highlight nil
        evil-want-fine-undo                 't))

(use-package evil-anzu :after evil)

(use-package evil-collection
  :after evil
  :custom
  (evil-collection-unimpaired-mode nil)
  (evil-collection-mode-list
   '(org rg (package-menu package) diff-mode evil-mc
         (pdf pdf-view) vterm flycheck ediff edebug
         help helpful bookmark eww))
  :init
  (evil-collection-init))

(use-package evil-goggles
  :after evil
  :custom 
  (evil-goggles-duration 0.3)
  :custom-face
  (evil-goggles-default-face ((t (:foreground "#fffff" :background "#ff40ff"))))
  :hook (evil-local-mode . evil-goggles-mode))

(use-package evil-commentary
  :after evil
  :ensure t
  :bind (:map evil-normal-state-map ("gc" . evil-commentary)))

(use-package evil-matchit
  :after evil 
  :config
  (define-key evil-normal-state-map (kbd "C-]") 'evilmi-jump-items)
  :hook (evil-local-mode . evil-matchit-mode)) ;

(use-package evil-surround
  :hook (evil-local-mode . evil-surround-mode) ;
  :after evil)

(use-package evil-indent-plus
  :after evil
  :init
  (define-key evil-inner-text-objects-map "i" 'evil-indent-plus-i-indent)
  (define-key evil-outer-text-objects-map "i" 'evil-indent-plus-a-indent)
  (define-key evil-inner-text-objects-map "I" 'evil-indent-plus-i-indent-up)
  (define-key evil-outer-text-objects-map "I" 'evil-indent-plus-a-indent-up)
  (define-key evil-inner-text-objects-map "J" 'evil-indent-plus-i-indent-up-down)
  (define-key evil-outer-text-objects-map "J" 'evil-indent-plus-a-indent-up-down))

(use-package evil-mc
  :bind
  ("C-x <up>" . evil-mc-make-cursor-move-previous-line)
  ("C-x <down>" . evil-mc-make-cursor-move-next-line)
  ("C-x <left>" . evil-mc-make-cursor-in-visual-selection-beg)
  ("C-x <right>" . evil-mc-make-cursor-in-visual-selection-end)
  ("C-x C-." . evil-mc-undo-all-cursors)
  ;; bound to g. collection
  :hook (evil-local-mode . evil-mc-mode))

(use-package evil-avy
  :hook (evil-local-mode . evil-avy-mode))

(use-package evil-owl
  :hook (evil-local-mode . evil-owl-mode))

(use-package evil-lion)
(use-package evil-numbers)

(use-package evil-args
  :hook (evil-local-mode . (lambda()(interactive)
                             (define-key evil-inner-text-objects-map "a" 'evil-inner-arg)
                             (define-key evil-outer-text-objects-map "a" 'evil-outer-arg)
                             (define-key evil-normal-state-map "L" 'evil-forward-arg)
                             (define-key evil-normal-state-map "H" 'evil-backward-arg)
                             (define-key evil-motion-state-map "L" 'evil-forward-arg)
                             (define-key evil-motion-state-map "H" 'evil-backward-arg))))

;; Add "gx" to exchange region easily
(use-package evil-exchange
  :general
  (:keymaps '(evil-normal-state-map evil-visual-state-map)
            "gx" 'evil-exchange))


(provide 'init-evil)

