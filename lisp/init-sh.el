(use-package shfmt
  :after sh-script
  :general
  (:keymaps  (general-leader sh-script-mode-map "lf" 'shfmt-buffer)))

(provide 'init-sh)
