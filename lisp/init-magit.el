(use-package magit
  :init
  (setopt transient-history-file (locate-user-emacs-file "auto-save-list/transient/transient.el")
          transient-values-file (locate-user-emacs-file "auto-save-list/transient/transient/values.el")
          transient-levels-file (locate-user-emacs-file "auto-save-list/transient/levels.el")
          magit-format-file-function #'magit-format-file-nerd-icons
          magit-diff-refine-hunk 'all
          magit-diff-refine-ignore-whitespace t
          magit-diff-highlight-trailing t
          magit-diff-paint-whitespace nil)
  :general
  (general-leader
    "gg"   #'(magit-status-here :wk "Magit status")
    "gc"   #'(my-magit-commit-buffer :wk "Magit commit buffer")
    "gC"   #'(magit-commit-create :wk "Magit commit create")
    "gp"   #'(magit-push-current-to-upstream :wk "Magit push current to upstream")
    "go"   #'(magit-pull-from-pushremote :wk "Magit pull from pushremote"))
  :commands (magit-read-repository magit-toplevel)
  :bind (("C-x v v" . my-magit-commit-buffer)
         ("C-x v -" . magit-pull-from-pushremote)
         ("C-x v =" . magit-diff-buffer-file)
         ("C-x v l" . magit-log-buffer-file)
         ("C-x v p" . magit-push-current-to-pushremote)
         ("C-x g" . magit-file-dispatch)
         ("C-c g" . magit-status))
  :bind
  (:map magit-refs-mode-map
        ("." . (lambda ()
                 (interactive)
                 (magit-checkout (magit-branch-at-point)))))
  (:map magit-status-mode-map
        ("C-d" . evil-scroll-down)
        ("C-u" . evil-scroll-up)
        (";" . (lambda ()
                 (interactive)
                 (magit-pull-from-pushremote nil)
                 (magit-push-current-to-pushremote nil)))
        ("C-k" . magit-discard)
        ("C-c j" . magit-status-jump)
        ("o" . magit-pull)
        ("p" . magit-push)
        ("k" . magit-section-backward)
        ("j" . magit-section-forward))

  (:map magit-log-mode-map
        ("C-d" . evil-scroll-down)
        ("C-u" . evil-scroll-up)
        ("C-k" . magit-discard)
        ("J"   . scroll-other-window)
        ("K"   . scroll-other-window-down)
        ("C-k" . magit-delete-thing)
        ("k"   . magit-section-backward)
        ("j"   . magit-section-forward))

  (:map magit-diff-mode-map
        ("C-d" . evil-scroll-down)
        ("C-u" . evil-scroll-up)
        ("C-k" . magit-discard)
        ("k" . magit-previous-line)
        ("j" . magit-next-line))
  (:map magit-revision-mode-map
        ("C-d" . evil-scroll-down)
        ("C-u" . evil-scroll-up)
        ("C-k" . magit-discard)
        ("k" . magit-previous-line)
        ("j" . magit-next-line))
  :commands (magit-process-file magit-list-repos-uniquify)
  :hook
  (magit-status-mode-hook my-magit-status-mode-hook)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1)
  :config
  (if (fboundp 'evil-ex-set-initial-state)
      (evil-set-initial-state 'git-commit-mode 'emacs))
  (defun my-magit-commit-buffer()
    (interactive)
    (if (not (magit-anything-modified-p nil (list (buffer-file-name))))
	    (message "Fileset is up-to-date")
      (let ((default-directory (magit-toplevel)))
	    (magit-run-git-with-editor
	     "add" (list (buffer-file-name)))
	    (magit-run-git-with-editor
	     "commit" (list "-v" (buffer-file-name)))
        )))

  (defun my-forge-show-pullreq-diff ()
    (interactive)
    (let ((remote-upstream
           (if (member "upstream" (magit-list-remotes))
               "upstream" "origin"))
          (pullreq-number
           (oref (forge-current-topic) number)))
      (magit-show-commit
       (format "%s/pr/%s" remote-upstream pullreq-number))))  
  (defun my-magit-status-mode-hook ()
    (local-set-key '[(control =)] 'magit-commit-create))
  (global-git-commit-mode))

(use-package vc
  :ensure nil
  :bind
  ("C-x v V" . vc-next-action)
  (:map vc-dir-git-mode-map
        ("C-d" . evil-scroll-down)
        ("C-u" . evil-scroll-up)
        ("C-k" . vc-dir-delete-entry)
        ("k" . vc-dir-previous-line)
        ("j" . vc-dir-next-line))
  :custom
  (vc-follow-symlinks t)
  (vc-make-backup-files t)
  :hook
  (log-edit . flyspell-mode)
  :config
  (if (fboundp 'evil-ex-set-initial-state)
      (evil-set-initial-state 'vc-git-log-edit-mode 'emacs)))

(use-package git-gutter
  :general
  (general-nmap
    "[v"    #'(git-gutter:next-hunk :properties (:repeat t :jump t))
    "]v"    #'(git-gutter:previous-hunk :properties (:repeat t :jump t)))
  (general-leader 
    "vs"    #'git-gutter:stage-hunk
    "vj"    #'(git-gutter:next-hunk :properties (:repeat t :jump t))
    "vx"    #'git-gutter:revert-hunk
    "vd"    #'git-gutter:popup-hunk
    "vk"    #'(git-gutter:previous-hunk :properties (:repeat t :jump t)))
  :hook
  (after-init . global-git-gutter-mode))

(use-package smerge-mode
  :ensure nil
  :general
  (general-leader "g m" '(hydra-smerge/body :wk "Hydra Merge" :repeat t))
  :config
  (defun my-smerge-first ()
    (interactive)
    (progn
      (goto-char (point-min))
      (smerge-next)))
  (defhydra hydra-smerge (:quit-key "C-g") "smerge"
    ("f" my-smerge-first "First")
    ("e" smerge-ediff "Ediff")
    ("j" smerge-next "Next")
    ("k" smerge-prev "Previous")
    ("u" smerge-keep-upper "Keep Upper")
    ("l" smerge-keep-lower "Keep Lower")
    ("b" smerge-keep-all "Keep Both")))

(use-package browse-at-remote
  :general
  (general-leader "vl" :wk "Browse at remote" #'browse-at-remote))

(provide 'init-magit)
