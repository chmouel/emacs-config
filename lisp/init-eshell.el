;;; init-eshell.el --- Eshell configuration -*- lexical-binding: t -*-

(use-package eshell
  :hook
  (eshell-post-command . eshell-add-aliases)
  :defer t
  :config
  (defun eshell-add-aliases ()
    "Doc-string."
    (dolist (var '(("d" "ls")
                   ("p" "cd -")
                   ("ll" "ls -l")
                   ("la" "ls -a")
                   ("s" "cd ..")))
      (add-to-list 'eshell-command-aliases-list var)))

  :custom
  (eshell-directory-name (locate-user-emacs-file "auto-save-list/eshell/")))

(use-package vterm-toggle
  :custom
  (vterm-always-compile-module 't)
  (vterm-toggle-fullscreen-p t)
  (vterm-toggle-scope 'dedicated)
  :bind
  ("s-`" . vterm-toggle)
  ("C-x x x" . vterm-toggle))

(use-package multi-vterm)

(use-package esh-module
  :defer t
  :ensure nil
  :custom
  (eshell-modules-list
   '(eshell-alias
     eshell-banner
     eshell-basic
     eshell-cmpl
     eshell-dirs
     eshell-glob
     eshell-hist
     eshell-ls
     eshell-pred
     eshell-prompt
     eshell-script
     ;; eshell-smart
     eshell-term
     eshell-unix
     )))


(use-package eshell-syntax-highlighting
  :defer t
  :ensure t
  :hook (eshell-mode . eshell-syntax-highlighting-mode))

(provide 'init-eshell)
