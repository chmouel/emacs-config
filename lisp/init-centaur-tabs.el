;;; init-centaur-tabs.el --- summary -*- lexical-binding: t -*-
(defun my-centaur-tabs-move-current-tab-to-first ()
  "Move the current tab to the first position in the tab list."
  (interactive)
  (let* ((bufset (centaur-tabs-current-tabset t))
         (tabs (centaur-tabs-tabs bufset))
         (current-tab (centaur-tabs-selected-tab bufset))
         (new-tabs))
    ;; Remove the current tab from its original position
    (setq new-tabs (remove current-tab tabs))
    ;; Insert the current tab at the beginning
    (push current-tab new-tabs)
    ;; Update the tabset with the reordered list
    (set bufset new-tabs)
    ;; Refresh the tab display
    (centaur-tabs-set-template bufset nil)
    (centaur-tabs-display-update)))

(use-package centaur-tabs
  :general
  (general-leader
    "jH" #'(centaur-tabs-move-current-tab-to-left :wk "Move tab to the left" :repeat t)
    "jL" #'(centaur-tabs-move-current-tab-to-right :wk "Move tab to the right" :repeat t)
    "ju" 'centaur-tabs-backward-group
    "jd" 'centaur-tabs-forward-group
    "jg" 'centaur-tabs-switch-group
    "jp" 'my-centaur-tabs-move-current-tab-to-first
    "bo" #'(centaur-tabs-kill-other-buffers-in-current-group :wk "Kill other buffers in group"))
  (general-nmap
    "<tab>" 'centaur-tabs-forward
    "<backtab>" 'centaur-tabs-backward)
  :bind
  ("C-c C-<SPC>" . centaur-tabs-ace-jump)
  ("C-<tab>" . centaur-tabs-forward)
  ("C-S-<tab>" . centaur-tabs-backward)
  ("C-S-<iso-lefttab>" . centaur-tabs-backward)
  ("C-<" . centaur-tabs-move-current-tab-to-left)
  ("C->" . centaur-tabs-move-current-tab-to-right)
  ("C-S-," . centaur-tabs-move-current-tab-to-left)
  ("C-S-." . centaur-tabs-move-current-tab-to-right)
  ("M-["               . centaur-tabs-backward)
  ("M-]"               . centaur-tabs-forward)
  ("C-M-<iso-lefttab>" . centaur-tabs-move-current-tab-to-left)
  ("C-M-<tab>" . centaur-tabs-move-current-tab-to-right)
  ("C-x C-<up>" . centaur-tabs-backward-group)
  ("C-x C-<down>" . centaur-tabs-forward-group)
  ("M-s-["             . centaur-tabs-backward-group)
  ("M-s-]"             . centaur-tabs-forward-group)
  ("C-s-<iso-lefttab>" . centaur-tabs-backward-group)
  ("C-s-<tab>"         . centaur-tabs-forward-group)
  :config
  (if (eq system-type 'gnu/linux)
      (setq centaur-tabs-modified-marker "●"))
  :custom
  (centaur-tabs-set-icons t)
  (centaur-tabs-cycle-scope 'tabs)
  (centaur-tabs-style "alternate")
  (centaur-tabs-set-modified-marker t)
  :hook
  (dired-mode . centaur-tabs-local-mode)
  (after-init . centaur-tabs-mode))

(provide 'init-centaur-tabs)
;;; init-centaur-tabs.el ends here
