(use-package reformatter
  :general
  (general-leader markdown-mode-map :states 'normal "lf" 'markdownlint-reformat-buffer)
  :if (executable-find "markdownlint")
  :config
  (reformatter-define markdownlint-reformat
    :program "markdownlint"
    :stdin nil
    :stdout nil
    :args (append `("-q" "-f" ,buffer-file-name))))

(defun my-copy-src-block-at-point (&optional copy-to-clipboard)
  "Mark or copy the content of the Markdown src block at point.
If COPY-TO-CLIPBOARD is non-nil, copy the content to clipboard.
Otherwise, just mark the region between delimiters.
If point is on the delimiter line (e.g. \"```elisp\"), it will skip it."
  (interactive "P")
  (let (start end)
    ;; If point is on a delimiter line, move to the next line.
    (when (looking-at " \t*```")
      (forward-line 1))
    
    ;; Search backward for the opening delimiter.
    (unless (re-search-backward " \t*```" nil t)
      (error "Could not find the beginning of a src block"))
    
    ;; Move to the line after the opening delimiter.
    (forward-line 1)
    (setq start (point))
    
    ;; Search forward for the closing delimiter.
    (unless (re-search-forward " \t*```" nil t)
      (error "Could not find the end of a src block"))
    
    (setq end (line-beginning-position))
    
    ;; Either copy or mark the region
    (if copy-to-clipboard
        (progn
          (kill-new (buffer-substring-no-properties start end))
          (message "Source block copied to kill ring."))
      ;; Mark the region
      (goto-char start)
      (set-mark (point))
      (goto-char end)
      (activate-mark)
      (message "Source block marked."))))

(use-package markdown-mode
  :custom
  (markdown-fontify-code-blocks-natively 't)
  (markdown-enable-highlighting-syntax 't)
  :general
  (general-leader :states 'normal "lv" ((lambda () (interactive) (my-copy-src-block-at-point t)) :wk "Copy src block"))
  (general-leader :states 'normal "lb" ((lambda () (interactive) (my-copy-src-block-at-point)) :wk "Mark src block"))
  (general-nmap markdown-mode-map :states '(normal visual)
    "g=" #'markdown-table-align
    "z'" #'markdown-insert-link)
  :hook
  ;; (markdown-mode . nb/markdown-unhighlight)
  (markdown-mode . abbrev-mode)
  (markdown-mode . electric-pair-mode))

(use-package poly-markdown)

(provide 'init-markdown)
