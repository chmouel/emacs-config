(use-package kaolin-themes)
(use-package emacs
  :config
  (blink-cursor-mode -1))

;; Jetbrains mono ligature
(use-package ligature
  :config
  (ligature-set-ligatures 'prog-mode '("-|" "-~" "---" "-<<" "-<" "--" "->" "->>" "-->" "///" "/=" "/=="
                                       "/>" "//" "/*" "*>" "***" "*/" "<-" "<<-" "<=>" "<=" "<|" "<||"
                                       "<|||" "<|>" "<:" "<>" "<-<" "<<<" "<==" "<<=" "<=<" "<==>" "<-|"
                                       "<<" "<~>" "<=|" "<~~" "<~" "<$>" "<$" "<+>" "<+" "</>" "</" "<*"
                                       "<*>" "<->" "<!--" ":>" ":<" ":::" "::" ":?" ":?>" ":=" "::=" "=>>"
                                       "==>" "=/=" "=!=" "=>" "===" "=:=" "==" "!==" "!!" "!=" ">]" ">:"
                                       ">>-" ">>=" ">=>" ">>>" ">-" ">=" "&&&" "&&" "|||>" "||>" "|>" "|]"
                                       "|}" "|=>" "|->" "|=" "||-" "|-" "||=" "||" ".." ".?" ".=" ".-" "..<"
                                       "..." "+++" "+>" "++" "[||]" "[<" "[|" "{|" "??" "?." "?=" "?:" "##"
                                       "###" "####" "#[" "#{" "#=" "#!" "#:" "#_(" "#_" "#?" "#(" ";;" "_|_"
                                       "__" "~~" "~~>" "~>" "~-" "~@" "$>" "^=" "]#"))
  :hook
  (after-init . global-ligature-mode))

;; Maybe we can do something a bit more smart with those config
(use-package doom-themes
  :custom
  (doom-one-light-brighter-modeline nil)
  (doom-one-light-padded-modeline t)
  (doom-one-light-brighter-comments t)
  (doom-nord-light-brighter-modeline nil)
  (doom-nord-light-padded-modeline t)
  (doom-nord-light-brighter-comments t)
  (doom-ayu-light-brighter-comments t)
  (doom-ayu-light-brighter-modeline nil)
  (doom-ayu-light-padded-modeline t)
  :custom-face
  (icomplete-first-match ((t (:inherit mode-line-emphasis)))))

(use-package doom-modeline
  :disabled
  :hook (after-init . doom-modeline-mode)
  :custom
  ((doom-modeline-env-version nil)
   (doom-modeline-buffer-encoding nil)
   (doom-modeline-workspace-name nil)
   (mode-line-right-align-edge 'right-margin)
   (doom-modeline-bar-width 0.5)
   (doom-modeline-height 10)))

(use-package minions
  :demand t
  :config
  (add-to-list 'minions-prominent-modes 'flymake-mode)
  (add-to-list 'minions-prominent-modes 'gptel-mode)
  (minions-mode 1))

(use-package all-the-icons-completion
  :hook (after-init . all-the-icons-completion-mode))

(provide 'init-theme)
