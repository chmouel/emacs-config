;;; init-llm-functions.el --- some functions for llm mode -*- lexical-binding: t -*-

(defvar my-llm-snippet-prefix "AI - ")
(defun my-llm-create-file-from-yasnippet ()
  "Create a new file from a Yasnippet snippet in `my-scratches` directory.
Only snippets that start with `my-llm-snippet-prefix` will be shown, and
the prefix will be removed from the created filename."
  (interactive)
  (let* ((snippets (yas--all-templates (yas--get-snippet-tables 'markdown-mode)))
         ;; Filter snippets that start with our prefix
         (filtered-snippets (cl-remove-if-not 
                             (lambda (snippet)
                               (string-prefix-p my-llm-snippet-prefix
                                                (yas--template-name snippet)))
                             snippets))
         (snippet-names (mapcar (lambda (snippet) (yas--template-name snippet)) filtered-snippets)))
    (if (null filtered-snippets)
        (message "No snippets found for markdown-mode with prefix '%s'." my-llm-snippet-prefix)
      (let* ((selected-snippet (completing-read "Choose a snippet: " snippet-names))
             (snippet (cl-find selected-snippet filtered-snippets :key #'yas--template-name :test #'string=))
             (timestamp (format-time-string "%a-%d-%b-%Y"))
             ;; Remove prefix from the filename
             (filename-without-prefix (substring (yas--template-name snippet) 
                                                 (length my-llm-snippet-prefix))))
        (when snippet
          (let ((file-path (expand-file-name
                            (concat my-scratches
                                    filename-without-prefix
                                    "-" timestamp
                                    ".md"))))
            (find-file file-path)
            (yas-expand-snippet (yas--template-content snippet))
            (gptel-mode)))))))

(defun my-gptel-query-dwim ()
  "A \"do-what-I-mean\" function to query gptel, adding context from the current buffer.
If a region is active, it adds the region to the gptel context.
Otherwise, it marks the current defun and adds that to the context.
It then switches to the most recent gptel buffer, or creates one if none exist."
  (interactive)
  (gptel-context-remove-all)
  (if (not (use-region-p))
      (mark-defun))
  (gptel-add)
  (let ((gptel-buffers
         (cl-remove-if-not
          (lambda (buf)
            (with-current-buffer buf
              (bound-and-true-p gptel-mode)))
          (buffer-list))))
    (if gptel-buffers
        (switch-to-buffer (car (last gptel-buffers)))
      (call-interactively 'gptel))))

(defun llm-buffer-p (buffer _ignored)
  "Return non-nil if BUFFER is a LLM buffer."
  (with-current-buffer buffer
    (or (bound-and-true-p copilot-chat-prompt-mode)
        (bound-and-true-p gptel-mode))))

(defun my-switch-to-gptel-buffer (&optional arg)
  "Switch to the most recent buffer with gptel-mode enabled or start it."
  (interactive "P")
  (let (target-buffer)
    (setq target-buffer (cl-find-if 
                         (lambda (buf)
                           (with-current-buffer buf
                             (bound-and-true-p gptel-mode)))
                         (buffer-list)))
    (unless target-buffer
      (call-interactively 'gptel))
    (pop-to-buffer target-buffer)))

(defun my-copy-last-src-block-from-gptel-buffer ()
  "Search for a buffer with gptel-mode enabled, go to its bottom, and copy the last Markdown src block.
The src block is assumed to be delimited by lines starting with \"```\"."
  (interactive)
  (let ((target-buffer nil))
    ;; Find a buffer where gptel-mode is active.
    (dolist (buf (buffer-list) target-buffer)
      (with-current-buffer buf
        (when (and (bound-and-true-p gptel-mode)
                   (not target-buffer))
          (setq target-buffer buf))))
    (unless target-buffer
      (error "No buffer with gptel-mode found"))
    (with-current-buffer target-buffer
      (save-excursion
        (goto-char (point-max))
        ;; Search backward for the closing delimiter of a src block.
        (unless (re-search-backward "^[ \t]*```" nil t)
          (error "No closing delimiter found in buffer %s" (buffer-name)))
        (let ((block-end (line-beginning-position)))
          ;; Search backward for the opening delimiter.
          (unless (re-search-backward "^[ \t]*```" nil t)
            (error "No opening delimiter found in buffer %s" (buffer-name)))
          (forward-line 1)
          (let ((block-start (point)))
            (kill-new (buffer-substring-no-properties block-start block-end))
            (message "Last src block copied from buffer %s" (buffer-name))))))))

(defun my-llm-save-file ()
  "Save to dated file with appropriate extension based on buffer mode.
Saves to `my-scratches` directory with format AI-DAY-DD-MON-YYYY.ext."
  (interactive)
  (let* ((mode (if (derived-mode-p 'markdown-mode) "md" "org"))
         (counter 1)
         (date-stamp (format-time-string "%a-%d-%b-%Y"))
         (base-name (format "AI-%s" date-stamp))
         (base-filename (expand-file-name (format "%s.%s" base-name mode) my-scratches))
         (filename base-filename))
    ;; Check if file exists and increment counter until we find a non-existent filename
    (while (file-exists-p filename)
      (setq filename (expand-file-name 
                      (format "%s-%d.%s" base-name counter mode) 
                      my-scratches))
      (setq counter (1+ counter)))
    (write-file filename)
    (message "Saved to %s" filename)))

(provide 'init-llm-functions)
