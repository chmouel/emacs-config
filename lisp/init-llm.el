;;; init-llm.el --- Init LLM -*- lexical-binding: t -*-
(require 'init-llm-functions)

(use-package gptel-context
  :after gptel
  :ensure nil
  :general
  (general-leader
    '(normal visual)
    "o c" '(:ignore t :wk "GPTel Context")
    "o c a" 'gptel-context-add
    "o c r" 'gptel-context-remove
    "o c s" '(lambda ()
               (interactive)
               (gptel-context-remove-all nil)
               (unless (use-region-p)
                 (mark-defun))
               (gptel-context-add)
               (my-switch-to-gptel-buffer)))
  :bind
  ("C-x a c". gptel-context-remove-all))

(use-package gptel
  :hook
  (gptel-mode . visual-line-mode)
  (gptel-pre-response . (lambda()
                          (interactive)
                          (unless (buffer-file-name)
                            (my-llm-save-file))))
  :general
  (general-leader
    '(normal visual)
    "o"   '(:ignore t :wk "GPTel")
    "o o" '(gptel :wk "Start GPTel")
    "o a" '(my-gptel-query-dwim :wk "Query DWIM")
    "o d" '(gptel-add :wk "Add to GPTel")
    "o m" '(gptel-menu :wk "GPTel menu")
    "o k" '(gptel-abort :wk "Abort GPTel")
    "o p" '((lambda () (interactive)
              (my-copy-last-src-block-from-gptel-buffer)
              (call-interactively 'evil-paste-before)) :wk "Paste last src block from GPTel")
    "o w"  '(my-copy-last-src-block-from-gptel-buffer :wk "Copy last src block from GPTel")
    "o k" '((lambda ()
              (interactive)
              (gptel-context-remove-all nil)
              (message "All GPTel context has been removed"))
            :wk "Remove all from GPTel Context")
    "o s" '(my-llm-save-file :wk "Save AI file"))
  :bind
  (:map gptel-mode-map
        ("C-c C-k" . gptel-abort)
        ("C-c C-m" . gptel-menu)
        ("C-<RET>" . gptel-send)
        ("C-<return>" . gptel-send)
        ("M-j" . (lambda()(interactive)(markdown-next-heading)(end-of-line)(evil-insert-state)))
        ("C-c C-c" . gptel-send))
  :custom
  (gptel-default-mode #'markdown-mode)
  (gptel-model 'gemini-2.0-pro-exp)
  :config
  ;; (add-to-list 'display-buffer-alist
  ;;              `(llm-buffer-p 
  ;;                (display-buffer-in-side-window)
  ;;                (side . right)))
  (setq gptel-backend
        (gptel-make-gemini "Gemini"
          :models '("gemini-2.0-flash"
                    "gemini-2.0-flash-lite-preview-02-05")
          :key (password-store-get "google/gemini-api")))

  (gptel-make-openai "Deepseek"
    :host "api.deepseek.com"
    :endpoint "/chat/completions"
    :stream t
    :key (password-store-get "deepseek/api")
    :models '("deepseek-chat" "deepseek-reasoner"))

  (gptel-make-ollama "Ollama"
    :host "localhost:11434"
    :stream t
    :models '("smollm:latest"
              "llama3.1:latest"
              "deepseek-r1:latest"
              "mistral-small:latest"
              "deepseek-r1:7b"
              "nomic-embed-text:latest"))
  (gptel-make-openai "Groq"
    :host "api.groq.com"
    :endpoint "/openai/v1/chat/completions"
    :stream t
    :key (password-store-get "groq/api")
    :models '("llama-3.3-70b-versatile"
              "llama-3.1-70b-versatile"
              "llama-3.1-8b-instant"
              "llama3-70b-8192"
              "llama3-8b-8192"
              "deepseek-r1-distill-qwen-32b"
              "deepseek-r1-distill-llama-70b-specdec"
              "qwen-2.5-coder-32b"
              "mixtral-8x7b-32768"
              "gemma-7b-it")))

(provide 'init-llm)

;;; init-llm.el ends here
