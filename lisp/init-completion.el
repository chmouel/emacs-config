;; Prettiness
(use-package nerd-icons-corfu)

(use-package vertico
  :custom
  (vertico-count-format '())
  :hook
  (after-init . vertico-reverse-mode)
  (after-init . vertico-mode)
  (minibuffer-setup . vertico-repeat-save)
  :bind
  (:map vertico-map
        ("C-s" . vertico-next)
        ("<return>" . vertico-exit)
        ("<C-return>" . vertico-exit-input)
        ("C-j" . (lambda () (interactive)
	               (if minibuffer--require-match
	        	       (minibuffer-complete-and-exit)
	                 (exit-minibuffer))))))

(use-package vertico-directory
  :ensure nil
  :after vertico
  :bind (:map vertico-map
              ("DEL"   . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word)
              ("C-w"   . vertico-directory-delete-word)
              ("RET"   . vertico-directory-enter)))

(use-package orderless
  :init
  (setq completion-styles '(orderless flex basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion))))
  (setq orderless-matching-styles
        '(orderless-initialism
          orderless-prefixes
          orderless-literal
          orderless-regexp)))


(use-package cape
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (defun my/orderless-dispatch-flex-first (_pattern index _total)
    (and (eq index 0) 'orderless-flex))
  (defun my/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless)))
  (add-hook 'orderless-style-dispatchers #'my/orderless-dispatch-flex-first nil 'local)
  (setq-local completion-at-point-functions (list (cape-capf-buster #'lsp-completion-at-point)))
  :custom
  (lsp-completion-provider :none) 
  :hook
  (lsp-completion-mode . my/lsp-mode-setup-completion))

(use-package corfu
  :custom
  (corfu-auto t)
  (corfu-cycle t)
  (corfu-preselect 'prompt)
  (corfu-quit-no-match t)
  (corfu-quit-at-boundary 'separator)
  (corfu-popupinfo-delay 0.3)
  (corfu-echo-documentation nil)
  :general
  (general-def
    :keymaps 'completion-in-region-mode
    :definer 'minor-mode
    :states 'insert
    :predicate 'corfu-mode
    "C-h" 'corfu-info-documentation
    "C-j" 'corfu-next
    "C-k" 'corfu-previous
    "C-c" 'keyboard-quit
    [backtab] 'corfu-previous
    [tab] 'corfu-next
    "<escape>" 'corfu-quit
    "C-m" 'corfu-insert)
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter)
  (defun corfu-move-to-minibuffer ()
    (interactive)
    (when completion-in-region--data
      (let ((completion-extra-properties corfu--extra)
            completion-cycle-threshold completion-cycling)
        (apply #'consult-completion-in-region completion-in-region--data))))
  (keymap-set corfu-map "M-m" #'corfu-move-to-minibuffer)
  (add-to-list 'corfu-continue-commands #'corfu-move-to-minibuffer)
  :hook
  (corfu-mode . corfu-popupinfo-mode)
  (after-init . global-corfu-mode))

(use-package corfu-terminal
  :if (not (display-graphic-p))
  :after (corfu)
  :init
  (corfu-terminal-mode +1))


(use-package nerd-icons-completion
  :after marginalia
  :config
  (nerd-icons-completion-mode)
  :hook
  ('marginalia-mode-hook . 'nerd-icons-completion-marginalia-setup))

(use-package marginalia
  :config
  (delete '(file marginalia-annotate-file builtin none) marginalia-annotator-registry)
  (delete '(buffer marginalia-annotate-buffer builtin none) marginalia-annotator-registry)
  (delete '(project-file marginalia-annotate-project-file builtin none) marginalia-annotator-registry)
  :custom
  (marginalia-annotators
   '(marginalia-annotators-light))
  :init
  (marginalia-mode +1))

(use-package vertico-prescient
  :ensure t
  :custom
  (prescient-save-file (locate-user-emacs-file "auto-save-list/prescient-cache.el"))
  (prescient-sort-length-enable nil)
  (prescient-filter-method '(literal fuzzy))
  (prescient-use-char-folding t)
  (prescient-use-case-folding 'smart)
  (prescient-sort-full-matches-first t)
  (prescient-sort-length-enable t)
  :config
  (vertico-prescient-mode)
  (prescient-persist-mode +1))

(use-package consult-dir
  :general (general-leader '(normal) "fd" #'(consult-dir :wk "Directory")))

(use-package consult-eglot
  :general
  (general-leader eglot-mode-map :states 'normal "lo" '(consult-eglot-symbols :wk "Browse Symbols with Consult")))

(use-package consult
  :custom
  (consult-async-min-input 2)
  (consult-narrow-key ">")
  (consult-async-refresh-delay 0.15)
  (consult-async-input-throttle 0.2)
  (consult-async-input-debounce 0.1)
  (completion-in-region-function #'consult-completion-in-region)
  (consult-project-root-function #'projectile-project-root)
  (xref-show-xrefs-function #'consult-xref)
  (xref-show-definitions-function #'consult-xref)
  :init
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)
  (advice-add #'register-preview :override #'consult-register-window)
  :bind
  ("C-S-o" . consult-outline)
  (:map isearch-mode-map
        ("C-l" . my-isearch-consult-line-from-isearch))
  :config
  (defun my-isearch-consult-line-from-isearch ()
    "Invoke `consult-line' from ace-isearch."
    (interactive)
    (let ((query (if isearch-regexp
		             isearch-string
		           (regexp-quote isearch-string))))
      (isearch-update-ring isearch-string isearch-regexp)
      (let (search-nonincremental-instead)
        (ignore-errors (isearch-done t t)))
      (consult-line query)))
  :bind
  ([remap goto-line] . #'consult-goto-line))

(use-package embark
  :bind (:map minibuffer-local-map
              ("M-o"     . embark-act)
              ("C-c C-c" . embark-export))
  :custom
  (prefix-help-command #'embark-prefix-help-command))

(use-package embark-consult
  :after (embark consult)
  :hook
  (embark-collect-mode . embark-consult-preview-minor-mode))

(unless (package-installed-p 'consult-vc-modified-files)
  (let ((localdir (expand-file-name (concat(getenv "GOPATH") "/src/github.com/chmouel/consult-vc-modified-files"))))
    (if (file-exists-p localdir)
        (package-vc-install-from-checkout localdir "consult-vc-modified-files"))))
(use-package consult-vc-modified-files
  :general (general-leader '(normal) "sm" #'(consult-vc-modified-files :wk "Modified files")))


(provide 'init-completion)
