(use-package flymake-yamllint
  :after yaml-mode
  :hook
  (yaml-mode . flymake-yamllint-setup))

(use-package yaml-ts-mode
  :preface
  (add-to-list 'major-mode-remap-alist '(yaml-mode . yaml-ts-mode))
  :mode
  (rx ".y" (optional "a") "ml" string-end)
  (rx (optional ".") "yamllint")
  :general
  (general-leader yaml-mode-map :states 'normal
    "lw" 'whitespace-cleanup
    "lb" 'format-all-buffer)
  :hook
  ;; (yaml-ts-mode . yaml-set-imenu-generic-expression)
  (yaml-ts-mode . eglot-ensure)
  (yaml-ts-mode . outline-minor-mode)
  (yaml-ts-mode . electric-pair-local-mode)
  (yaml-ts-mode . highlight-indentation-mode)
  :config
  (setq-local outline-regexp "^ *\\([A-Za-z0-9_-]*: *[>|]?$\\|-\\b\\)")
  (font-lock-add-keywords
   'yaml-ts-mode
   '(("\\($(\\(workspaces\\|context\\|params\\)\.[^)]+)\\)" 1 'font-lock-constant-face prepend)
     ("kind:\s*\\(.*\\)\n" 1 'font-lock-keyword-face prepend)))
  :bind
  (:map yaml-ts-mode-map
        ("C--" . (lambda () (interactive) (call-interactively 'highlight-indentation-current-column-mode)))
        ("C-S-f" . format-all-buffer)
        ("C-=" . whitespace-cleanup)))

(use-package yaml-pro
  :after yaml-ts-mode
  :hook (yaml-ts-mode . yaml-pro-mode))

(provide 'init-yaml)
