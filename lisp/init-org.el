(defconst org-directory "~/Sync/orgs")

(defconst org-todo-file (expand-file-name "todo.org" org-directory))
(defconst org-notes-file (expand-file-name "notes.org" org-directory))
(defconst org-links-file (expand-file-name "links.org" org-directory))
(defconst org-snippet-file (expand-file-name "snippets.org" org-directory))
(defconst org-italian-file (expand-file-name "italian.org" org-directory))

(use-package denote
  :disabled
  :general
  (general-leader
    "n d" '(denote :which-key "Denote"))
  :custom
  (denote-modules '(project xref ffap))
  (denote-known-keywords '("work" "nix" "linux" "shell" "python" "neovim"
                           "keyboard" "life" "travel" "emacs" "programming"
                           "pipelinesascode" "openshift" "tektoncd"))
  (denote-directory "~/Sync/denotes/"))

(use-package consult-notes
  :disabled
  :hook (after-init . consult-notes-denote-mode)
  :general
  (general-leader
    "n f" '(consult-notes :which-key "Denote Open")))

(use-package evil-org
  :hook (org-mode . evil-org-mode))

(use-package org-modern-indent
  :vc (:url  "https://github.com/jdtsmith/org-modern-indent" :branch "main")
  :hook (org-mode . org-modern-indent-mode))

(use-package org-capture
  :ensure nil
  :commands (org-capture)
  :general
  (general-leader
    "n" '(:ignore t :which-key "Notes")
    "n c" '(org-capture :which-key "Capture menu")
    "n l" '((lambda() (interactive)(org-capture nil "l")) :which-key "Capture a link")
    "n i" '((lambda() (interactive)(find-file org-links-file)) :which-key "Show Link file")
    "n t" '((lambda() (interactive)(org-capture nil "t")) :which-key "Capture a TODO")
    "n b" '((lambda() (interactive)(find-file org-todo-file)) :which-key "Show TODO file")
    "n n" '((lambda() (interactive)(org-capture nil "n")) :which-key "Capture a note")
    "n m" '((lambda() (interactive)(find-file org-notes-file)) :which-key "Show Notes file"))
  :config
  (defun my-capture-code-snippet ()
    (format
     "file:%s::%s

In function ~%s~
#+BEGIN_SRC %s
%s
#+END_SRC"
     (abbreviate-file-name buffer-file-name)
     (if (region-active-p)
         (format "%s-%s"
                 (line-number-at-pos
                  (region-beginning))
                 (line-number-at-pos (region-end)))
       (line-number-at-pos))
     (cond
      ((fboundp 'which-function)
       (which-function))
      ((eq major-mode 'go-mode)
       (save-excursion
         (go-goto-function-name)
         (substring-no-properties (thing-at-point 'sexp))))
      ((eq major-mode 'python-mode) ;; pytest regexp
       (save-excursion
         (re-search-backward
          "^[ \t]\\{0,4\\}\\(class\\|\\(?:async \\)?def\\)[ \t]+\\([a-zA-Z0-9_]+\\)" nil t)
         (buffer-substring-no-properties (match-beginning 2) (match-end 2)))))
     (replace-regexp-in-string "-mode$" "" (symbol-name major-mode))
     (if (region-active-p)
         (buffer-substring-no-properties (region-beginning) (region-end))
       (buffer-substring-no-properties
        (line-beginning-position)
        (line-end-position)))))
  (setq org-capture-templates
        '(("t" "TODO" entry
           (file+olp org-todo-file "TODOS")
           "* TODO %U\n%?"
           :empty-lines 1)
          ("n" "Notes" entry
           (file+olp org-notes-file "Notes")
           "* %U %?"
           :empty-lines 1)
          (";" "Link>Ask" entry
           (file+olp org-links-file "LINKS")
           "* %U [[%^{link-url}][%^{link-description}]] %?"
           :empty-lines 1)
          ("l" "Link>clipboard" entry
           (file+olp org-links-file "LINKS")
           "* %U %c %? %^g"
           :empty-lines 1)
          ("L" "Link>simple" entry
           (file+olp org-links-file "LINKS")
           "* %U %?"
           :empty-lines 1)
          ("p" "Work>TODO" entry
           (file+olp org-todo-file "Work")
           "* TODO %?\n\n%(with-current-buffer (org-capture-get :original-buffer) (my-capture-code-snippet))\n
Captured at %U"
           :empty-lines 1)

          ("P" "Work>Notes" entry
           (file+olp org-notes-file "Work")
           "* %?\nCaptured at %U"
           :empty-lines 1)


          ("Pl" "Protocol Link" entry (file+olp org-notes-file "Random") "* %?[[%:link][%:description]]\n")
          ("Pn" "Protocol Notes" entry (file+olp org-notes-file "Random") "* [[%:link][%:description]]\n#+BEGIN_QUOTE %?\n%:initial\n#+END_QUOTE\n\n" :empty-lines 1))))

(use-package org-bullets
  :ensure t
  :custom-face
  (outline-1 ((t (:height 1.15))))
  (outline-2 ((t (:height 1.13))))
  :hook (org-mode . org-bullets-mode))

(use-package org-tempo
  :ensure nil
  :after org)

(use-package org
  :ensure nil
  :if window-system
  :hook
  (org-mode . (lambda()(interactive)(require 'org-tempo))))

(use-package org
  :ensure nil
  :general
  (general-leader org-mode-map :states 'normal "lo" 'consult-org-heading)
  :mode (("\\.org$" . org-mode))
  :commands (org-agenda org-capture)
  :custom
  (org-return-follows-link t)
  (org-reverse-note-order t)
  (org-archive-location "archives.org::* Archives %s")
  (org-use-speed-commands t)
  (org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)" "CANCELED(c@/!)" "ONEDAY(o)" )))
  (org-todo-state-tags-triggers '(("CANCELLED" ("CANCELLED" . t))
                                  ("WAITING" ("WAITING" . t))
                                  (done ("WAITING"))
                                  ("TODO" ("WAITING") ("CANCELLED"))
                                  ("NEXT" ("WAITING") ("CANCELLED"))
                                  ("DONE" ("WAITING") ("CANCELLED"))))
  (org-use-tag-inheritance t)
  (org-tag-alist '(("linux")
                   ("emacs")
                   ("programming")
                   (:startgroup . nil)
                   ("pipelinesascode") ("openshift") ("redhat") ("tektoncd")
                   (:endgroup . nil)))
  (org-special-ctrl-a/e t)
  (org-special-ctrl-o nil)
  (org-startup-truncated nil)
  (org-completion-use-ido t)
  (org-todo-keyword-faces
   '(("IDEA" . (:foreground "GoldenRod" :weight bold))
     ("NEXT" . (:foreground "IndianRed1" :weight bold))
     ("STARTED" . (:foreground "OrangeRed" :weight bold))
     ("WAITING" . (:foreground "coral" :weight bold))
     ("CANCELED" . (:foreground "LimeGreen" :weight bold))
     ("DELEGATED" . (:foreground "LimeGreen" :weight bold))
     ("SOMEDAY" . (:foreground "LimeGreen" :weight bold))))
  (org-adapt-indentiation nil))

(use-package org-download
  :hook (org-mode . org-download-enable)
  :after org)

(use-package org-block-capf
  :vc (:url   "https://github.com/xenodium/org-block-capf":branch "main")
  :hook
  (org-mode . org-block-capf-add-to-completion-at-point-functions)
  :bind
  (:map org-mode-map
        ("C-." . org-block-capf)))

(use-package org-reverse-datetree
  :after org)

(use-package htmlize)

(provide 'init-org)
