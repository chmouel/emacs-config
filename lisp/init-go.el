(use-package go-mode
  :commands
  (gofmt go-goto-imports))

(use-package gotest-ts
  :commands (gotest-ts-run-dwim))

(use-package flymake-golangci
  :disabled
  :custom
  (flymake-golangci-fast t)
  (flymake-golangci-tests t)
  :hook
  (go-ts-mode . 'flymake-golangci-load))

(use-package dape
  :preface
  (defvar dape-configs nil)
  (setq dape-default-breakpoints-file (expand-file-name "dape-breakpoints.el" user-emacs-directory))
  :autoload (dape dape-stop dape-restart dape-breakpoint-toggle dape-next dape-step-in dape-step-out)
  :after go-ts-mode
  :commands
  (my-dape-go-test-at-point)
  :bind
  (:map go-ts-mode-map
        ("C-x C-a a" . my-dape-breakpoint-remove-others)
        ("C-x C-a u" . (lambda () (interactive) (dape-breakpoint-load)))
        ("C-x C-a b" . my-dape-breakpoint-toggle-and-hl))
  :general
  (:keymaps 'go-ts-mode-map
            (general-define-key
             "<f5>" 'dape
             "S-<f5>" 'dape-stop
             "C-S-<f5>" 'dape-restart
             "<f9>" 'my-dape-breakpoint-toggle-and-hl
             "<f10>" 'dape-next
             "<f11>" 'dape-step-in
             "<f12>" 'dape-step-out))
  (general-leader "td" 'my-dape-go-test-at-point)
  (general-leader "d" #'((lambda()(interactive)(general-simulate-key "C-x C-a")) :wk "Dape"))
  :hook
  (go-ts-mode . dape-breakpoint-global-mode)
  (go-ts-mode . (lambda()
                  (interactive)
                  (if (string-suffix-p "_test.go"  (buffer-name))
                      (setq-local dape-command '(delve-unit-test)))))
  :custom
  (dape-info-buffer-window-groups '((dape-info-scope-mode)(dape-info-breakpoints-mode)))
  :config
  (defun my-dape-breakpoint-remove-others()
    "Remove all breakpoints and add one at the current line."
    (interactive)
    (dape-breakpoint-remove-all)
    (my-dape-breakpoint-toggle-and-hl))
  (defun highlight-current-line ()
    "Highlight the current line using a custom face."
    (interactive)
    (let ((overlay (make-overlay (line-beginning-position) (line-end-position))))
      (overlay-put overlay 'face 'highlight-current-line-face)
      (overlay-put overlay 'evaporate t)))

  (defface highlight-current-line-face
    '((t (:background "grey80" :foreground "yellow")))
    "Face for highlighting the current line.")

  (defun clear-current-line-highlights ()
    "Clear all current line highlights."
    (interactive)
    (remove-overlays nil nil 'face 'highlight-current-line-face))

  (defun my-dape-breakpoint-toggle-and-hl ()
    "Add or remove breakpoint at current line."
    (interactive)
    (if (cl-member nil (dape--breakpoints-at-point)
                   :key #'dape--breakpoint-type)
        (progn
          (clear-current-line-highlights)
          (dape-breakpoint-remove-at-point))
      (progn
        (dape--breakpoint-place)
        (highlight-current-line))))
  (defun my-dape-go-test-at-point ()
    (interactive)
    (dape (dape--config-eval-1
           `(modes (go-mode go-ts-mode)
                   ensure dape-ensure-command
                   fn dape-config-autoport
                   command "dlv"
                   command-args ("dap" "--listen" "127.0.0.1::autoport")
                   command-cwd dape-cwd-fn
                   port :autoport
                   :type "debug"
                   :request "launch"
                   :mode "test"
                   :buildFlags ["-tags=e2e"]
                   :cwd (lambda() default-directory)
                   :program (lambda () (concat "./" (file-relative-name default-directory (funcall dape-cwd-fn))))
                   :args (lambda ()
                           (when-let* ((test-name (gotest-ts-get-subtest-ts)))
                             (if test-name `["-test.run" ,test-name]
                               (error "No test selected")))))))))

(use-package go-gen-test
  :config
  (let ((gentest-executable (executable-find "gotests")))
    (setq go-gen-test-executable gentest-executable)))

(use-package gotest
  :commands (my-gotest-maybe-ts-run go-test--get-current-test-info)
  :after go-ts-mode
  :config
  (defun my-go-test-current-project()
    (interactive)
    (let ((default-directory (project-root (project-current t))))
      (go-test-current-project)))

  (defun my-gotest-maybe-ts-run()
    (interactive)
    (when (string-match "_test\\.go" (buffer-file-name))
      (let ((gotest (my-gotest-get-current-test)))
        (go-test--go-test (concat "-run " gotest " .")))))
  :custom
  (go-test-verbose t))

(use-package reformatter
  :config
  (reformatter-define goimports
    :program (executable-find "goimports")
    :stdin nil
    :input-file (reformatter-temp-file-in-current-directory)
    :args `("-e" ,input-file)))

(use-package go-playground
  :after go-ts-mode
  :bind (:map
         go-ts-mode-map
         ("C-c ," . my-jump-go-playground-snippet))
  :config
  (defun my-jump-go-playground-snippet (snippet)
    (interactive
     (list
      (completing-read
       "Snippet: "
       (mapcar
        (lambda (x)
          (f-base x))
        (f-directories go-playground-basedir)))))
    (find-file
     (concat
      go-playground-basedir "/" snippet "/"
      (s-replace-regexp "-at.*" "" snippet) ".go")))
  :preface
  (setq go-playground-init-command "go mod init github.com/chmouel/$(basename $PWD|sed 's/-at.*//')"
        go-playground-ask-file-name t
        go-playground-basedir "~/Sync/goplay"))

(use-package go-ts-mode
  :custom
  (gofmt-command "goimports")
  :general
  (general-leader "lp" #'(my-go-import-add :wk "Add Go import"))
  :bind (:map go-ts-mode-map
              ("C-c C-r" . go-remove-unused-imports)
              ("C-c d" . godoc-at-point)
              ("C-S-r" . go-run)
              ("C-c t" . ff-find-other-file))
  :hook
  (go-ts-mode . my-go-mode-hook)
  (go-mode . my-go-mode-hook)
  :config
  (setq go-other-file-alist
        '(("_test\\.go\\'" (".go"))
          ("\\.go\\'" ("_test.go"))))
  (defun my-go-mode-hook ()
    (require 'dape)
    (goimports-on-save-mode +1)
    ;; not in go-ts-mode
    (setq ff-other-file-alist 'go-other-file-alist)
    (if (and buffer-file-name
             (string-match "_test\\'"
                           (file-name-sans-extension buffer-file-name)))
        (progn
          (local-set-key (kbd "C-S-a") 'my-go-test-current-project)
          (local-set-key (kbd "C-S-y") 'go-test-current-file)
          (local-set-key (kbd "C-S-d") 'my-dape-go-test-at-point)
          (local-set-key (kbd "C-S-r") 'gotest-ts-run-dwim))))
  (defun my-go-import-add ()
    (interactive)
    (go-import-add nil (read-from-minibuffer "Import: ")))

  (defun my-find-unique-go-imports (&optional directory)
    "Find unique Go imports using ripgrep in DIRECTORY or current project root.
Returns a sorted list of unique import strings."
    (interactive)
    (let* ((default-directory (or directory (project-root (project-current t))))
           (rg-command "rg --no-heading --no-filename --no-line-number --multiline --multiline-dotall 'import[ ]*\\(\\s*([a-z\"].*?)\\)' -r '$1' -g '*.go' .")
           (rg-output (shell-command-to-string rg-command))
           (import-table (make-hash-table :test 'equal)))
      (with-temp-buffer
        (insert rg-output)
        (goto-char (point-min))
        (while (re-search-forward "^[ 	]?\\(.*\"\\)$" nil t)
          (puthash (match-string 1) t import-table)))
      (sort (hash-table-keys import-table) #'string<)))

  (defun my--go-import-add (import)
    (interactive)
    (save-excursion
      (let (import-start)
        (goto-char (point-min))
        (if (re-search-forward (concat "^[[:space:]]*//[[:space:]]*import " import "$") nil t)
            (uncomment-region (line-beginning-position) (line-end-position))
          (cl-case (go-goto-imports)
            (fail (message "Could not find a place to add import."))
            (block-empty
             (insert "\n\t" line "\n"))
            (block
             (save-excursion
               (re-search-backward "^import (")
               (setq import-start (point)))
             (unless (re-search-backward (concat "^[[:space:]]*" import "$")  import-start t)
               (insert "\n\t" import)))
            (single (insert "import " import "\n"))
            (none (insert "\nimport (\n\t" import "\n)\n")))))))

  (defun my-go-import-add()
    (interactive)
    (my--go-import-add (completing-read "Select a Go import: " (my-find-unique-go-imports)))))

  (provide 'init-go)
