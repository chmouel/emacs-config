(defun my-recompile (args)
  (interactive "P")
  (cond
   ((eq major-mode #'emacs-lisp-mode)
    (call-interactively 'eros-eval-defun))
   ((bound-and-true-p my-vterm-command)
    (my-vterm-execute-region-or-current-line my-vterm-command))
   ((get-buffer "*compilation*")
    (with-current-buffer "*compilation*"
      (recompile)))
   ((get-buffer "*Go Test*")
    (with-current-buffer "*Go Test*"
      (recompile)))
   ((and (or (eq major-mode #'go-mode) (eq major-mode #'go-ts-mode))
         buffer-file-name
         (string-match "_test\\'" (file-name-sans-extension buffer-file-name)))
    (my-gotest-maybe-ts-run))
   ((and (get-buffer "*cargo-test*")
         (boundp 'my-rustic-current-test-compile)
         my-rustic-current-test-compile)
    (with-current-buffer "*cargo-test*"
      (rustic-cargo-test-run my-rustic-current-test-compile)))
   ((get-buffer "*cargo-run*")
    (with-current-buffer "*cargo-run*"
      (rustic-cargo-run-rerun)))
   ((get-buffer "*pytest*")
    (with-current-buffer "*pytest*"
      (recompile)))
   ((or (eq major-mode #'python-mode) (eq major-mode #'python-ts-mode))
    (compile (concat python-shell-interpreter " " (buffer-file-name))))
   ((call-interactively 'compile))))

(use-package flymake
  :ensure nil
  :defer t
  :bind
  (:map
   flymake-diagnostics-buffer-mode-map
   ("p" .
    (lambda ()
      (interactive)
      (previous-line)
      (save-excursion (flymake-show-diagnostic (point)))))
   ("n" .
    (lambda ()
      (interactive)
      (next-line)
      (save-excursion (flymake-show-diagnostic (point)))))
   (:map
    flymake-project-diagnostics-mode-map
    ("p" .
     (lambda ()
       (interactive)
       (previous-line)
       (save-excursion (flymake-show-diagnostic (point)))))
    ("n" .
     (lambda ()
       (interactive)
       (next-line)
       (save-excursion (flymake-show-diagnostic (point)))))))
  :hook (prog-mode . flyspell-prog-mode) (prog-mode . flymake-mode))

(use-package topsy
  :general
  (general-leader
    prog-mode-map
    :states 'normal "j h" '(topsy-mode :wk "Topsy")))

(use-package emacs
  :ensure nil
  :defer t
  :custom (next-error-message-highlight t)
  :config
  (add-hook
   'before-save-hook
   (lambda ()
     (interactive)
     (when (or (derived-mode-p 'markdown-mode) (derived-mode-p 'yaml-mode))
       (delete-trailing-whitespace))))
  :general
  (general-leader
    "RET" #'(my-recompile :wk "My compile DWIM")
    "M-RET" #'(compile :wk "Compile command line"))
  :bind
  (:map
   prog-mode-map
   ("<backtab>" . yas-insert-snippet)
   ("C-M-<return>" . compile)
   ("C-c C-c" . my-recompile)
   ([remap newline] . newline-and-indent)))

(use-package iedit
  :bind
  ("C-;" . iedit-mode)
  :general
  (general-leader
    prog-mode-map
    :states 'normal "l w" '(iedit-mode :wk "Interactive Edit")))

(use-package ansi-color
  :config
  (defun my-colorize-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  :hook (compilation-filter . my-colorize-compilation-buffer))

(use-package highlight-indentation)

(use-package highlight-parentheses
  :hook (prog-mode . highlight-parentheses-mode))

(use-package highlight-numbers :hook (prog-mode . highlight-numbers-mode))

(use-package toggle-quotes
  :bind
  ("C-'" . toggle-quotes)
  :general
  (general-leader
    prog-mode-map
    :states 'normal "t q" '(toggle-quotes :wk "Toggle Quotes")))

(use-package multi-compile
  :custom
  (multi-compile-history-file
   (locate-user-emacs-file "auto-save-list/multi-compile.cache")))

(use-package git-link
  :disabled
  :custom
  (git-link-open-in-browser t)
  :general
  (general-leader :states 'normal "v g" '(git-link :wk "Open in SCM interface")))

(use-package indent-bars
  :if (eq system-type 'gnu/linux)
  :hook
  (python-mode . indent-bars-mode)
  (yaml-mode . indent-bars-mode)
  :config
  (require 'indent-bars-ts)
  :custom
  (indent-bars-no-descend-lists t)
  (indent-bars-treesit-support t)
  (indent-bars-treesit-ignore-blank-lines-types '("module"))
  (indent-bars-treesit-scope '((python function_definition class_definition for_statement
	                                   if_statement with_statement while_statement))))

(use-package jsonian)

;; Dockerfile
(use-package dockerfile-mode :mode "\\(Docker\\|Container\\)file\\'")

;; Lua
(use-package lua-mode)

;; Format buffers!
(use-package format-all
  :init
  (setq format-all-formatters '(("Python" ruff)))
  (add-hook
   'before-save-hook
   (lambda ()
     (interactive)
     (when (derived-mode-p 'python-ts-mode)
       (format-all-buffer))))
  :hook
  (prog-mode . format-all-mode)

  :general
  (general-leader prog-mode-map :states 'normal
    "lF" #'((lambda()(interactive) (call-interactively 'format-all-buffer)) :wk "Format all buffer"))
  :commands (format-all-mode format-all-ensure-formatter))

(use-package restclient
  :commands (restclient-mode)
  :mode ("\\.rest\\'" . restclient-mode)
  :config
  (add-to-list 'restclient-content-type-modes '("application/json" . json-mode)))

(use-package bm
  :custom-face (bm-face ((t (:background "LightCyan1" :foreground "black")))))

(use-package hl-todo :hook (prog-mode . hl-todo-mode))

(use-package prog-mode
  :ensure nil
  :hook
  (after-init . which-function-mode))

(provide 'init-programming)
