;;; init-dired.el ---                                -*- lexical-binding: t; -*-
(use-package dired-x
  :ensure nil
  :hook ((dired-mode . dired-omit-mode))
  :custom
  ((dired-omit-files
    (concat "^\\.\\|^\\.?#\\|^\\.$\\|^\\.\\.$\\|"
            "^Thumbs.db$\\|\\.svn$\\|\\.git\\(ignore\\)?\\|"
            "\\.pyc$\\|^\\.coverage$\\|^TAGS$\\|^tags$\\|"
            "\\.class$\\|\\.DS_Store\\|\\.localized$\\|__pycache__$")))
  :config
  (setq dired-omit-verbose nil)
  ;; hide backup, autosave, *.*~ files
  ;; omit mode can be toggled using `C-x M-o' in dired buffer.
  (setq dired-omit-files
        (concat dired-omit-files "\\|^.DS_STORE$\\|^.projectile$\\|^.git$")))

(use-package nerd-icons-dired
  :hook (dired-mode . nerd-icons-dired-mode))

(use-package dired-sidebar
  :disabled
  :general
  (general-leader "fe" '(dired-sidebar-toggle-sidebar :wk "Toggle dired sidebar"))
  :custom
  (dired-sidebar-close-sidebar-on-file-open 't)
  (dired-sidebar-theme 'nerd))


;; Dired
(use-package dired
  :ensure nil
  :general
  (general-leader "sf"
    '((lambda ()
        (interactive)
        (find-lisp-find-dired default-directory (completing-read "Find file in current dir (Regexp): " nil))) :wk "Find find in dired")
    "sF" '(find-lisp-find-dired :wk "Find find in dired"))
  :hook
  (dired-mode . my-dired-mode-hook)
  (dired-mode . dired-hide-details-mode)
  (dired-mode . dired-sort-toggle-or-edit)
  :config
  (if (executable-find "gls")
      (setq insert-directory-program "gls"))
  (defun my-dired-mode-hook ()
    (when (featurep 'tooltip) (tooltip-mode 0)))
  :custom
  (dired-dwim-target t)
  (dired-isearch-filenames t)
  (dired-kill-when-opening-new-dired-buffer 't)
  (dired-hide-details-hide-symlink-targets 'nil)
  (dired-hide-details-hide-information-lines 'nil)
  (dired-create-destination-dirs-on-trailing-dirsep 't)
  (dired-create-destination-dirs 't)
  :bind
  (:map dired-mode-map
        ("W"   . browse-url-of-dired-file)
        ("a"   . dired-create-empty-file)
        ("TAB" . tab-next)
        ("O"   . dired-omit-mode)
        ("E"   . wdired-change-to-wdired-mode)
        ("s"   . dired-up-directory)
        ("h"   . dired-up-directory)
        ("l"   . dired-find-file)
        ("f"   . dired-goto-file)
        ("j"   . dired-next-line)
        ("k"   . dired-previous-line)))

(use-package dired-collapse
  :hook
  (dired-mode . dired-collapse-mode))

(use-package dired-narrow
  :bind
  (:map dired-mode-map
        ("/" . dired-narrow-fuzzy)))

(use-package dired-quick-sort
  :hook
  (dired-mode . dired-quick-sort-setup))

(provide 'init-dired)
;;; init-dired.el ends here
