;;; init-python.el --- summary -*- lexical-binding: t -*-
(use-package python-docstring :diminish :after python-ts-mode)
(use-package pytest :after python-ts-mode)
(use-package hungry-delete :diminish :after python-ts-mode)

(use-package py-isort
  :hook
  (before-save . (lambda ()
                   (interactive)
                   (when (derived-mode-p 'python-ts-mode)
                     (py-isort-buffer))))
  :bind
  (:map python-ts-mode-map
        ("C-c I" . python-import-symbol-at-point)
        ("C-c i" . my-py-import-add))
  :config
  (defun my-py-insert-import (arg import)
    (save-excursion
      (goto-char (point-min))
      (while (or (python-info-current-line-comment-p) (python-info-docstring-p))
        (forward-line))
      (insert
       (concat
        "import " import
        (if arg
            (concat " as " (read-from-minibuffer "Import as: "))))
       "\n")))

  (defun my-py-import-add (arg import)
    (interactive (list current-prefix-arg (read-from-minibuffer "Package: ")))
    (my-py-insert-import arg import)
    (py-isort-buffer)))

(use-package python
  :ensure nil
  :custom (python-shell-interpreter "python3") (python-interpreter "python3")
  :bind
  (:map
   python-ts-mode-map
   ("C-c t" . ff-find-other-file)
   ("M-q" . fill-paragraph))
  :config
  (defun my-python-mode-hook ()
    (if (fboundp 'flycheck-add-next-checker)
        (flycheck-add-next-checker 'lsp '(warning . python-pylint)))
    (if
        (and buffer-file-name
             (string-match
              "\\(^test_\\|_test$\\)"
              (file-name-base (file-name-sans-extension buffer-file-name))))
        (progn
          (local-set-key '[(control return)] 'my-recompile)
          (local-set-key (kbd "C-S-r") 'pytest-one))))
  :hook
  ((python-ts-mode . hungry-delete-mode)
   (python-ts-mode . my-python-mode-hook)))

(provide 'init-python)
;;; init-python.el ends here
