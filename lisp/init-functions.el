;;; init-function.el --- chmou Emacs functions -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Go to GNUS
(defun my-switch-to-gnus ()
  "Select the tab or switch to gnus."
  (interactive)
  (let ((tab-index (or (tab-bar--tab-index-by-name "*Group*") -1)))
    (if (= tab-index -1)
        (progn
          (tab-bar-select-tab tab-index)
          (gnus)))
    (tab-bar-select-tab (1+ tab-index))))

;; https://github.com/doomemacs/doomemacs/blob/9c8cfaadde1ccc96a780d713d2a096f0440b9483/lisp/doom-keybinds.el#L81
(defvar doom-escape-hook nil
  "A hook run when C-g is pressed (or ESC in normal mode, for evil users).

More specifically, when `doom/escape' is pressed. If any hook returns non-nil,
all hooks after it are ignored.")

(defun doom/escape (&optional interactive)
  "Run `doom-escape-hook'."
  (interactive (list 'interactive))
  (let ((inhibit-quit t))
    (cond ((minibuffer-window-active-p (minibuffer-window))
           ;; quit the minibuffer if open.
           (when interactive
             (setq this-command 'abort-recursive-edit))
           (abort-recursive-edit))
          ;; Run all escape hooks. If any returns non-nil, then stop there.
          ((run-hook-with-args-until-success 'doom-escape-hook))
          ;; don't abort macros
          ((or defining-kbd-macro executing-kbd-macro) nil)
          ;; Back to the default
          ((unwind-protect (keyboard-quit)
             (when interactive
               (setq this-command 'keyboard-quit)))))))

(global-set-key [remap keyboard-quit] #'doom/escape)

;; I have a bunch of different 'profiles' for kubernetes by different cluster so
;; i don't mess between things
;; This allow me to set the KUBECONFIG variable between those easily
;; TODO: add the current profile in modeline
(defun my-switch-kubeconfig-env (&optional kubeconfig)
  (interactive (list
                (completing-read "Kubeconfig: "
                                 (mapcar
                                  (lambda (x)
                                    (replace-regexp-in-string
                                     "^config\." ""
                                     (file-name-nondirectory
                                      (directory-file-name x))))
                                  (directory-files-recursively
                                   (expand-file-name "~/.kube") "^config\."))
                                 nil t)))
  (setq kubeconfig (expand-file-name (format "~/.kube/config.%s" kubeconfig)))
  (if (file-exists-p kubeconfig)
      (setenv "KUBECONFIG" kubeconfig)
    (error "Cannot find kubeconfig: %s" kubeconfig)))

;;https://www.emacswiki.org/emacs/NavigatingParentheses

;; Sync directories with gh repo sync, gh repo sync is not great, may just adapt
;; in "pure" magit commands
(defun my-sync-dir ()
  (interactive)
  (require 'magit-repos)
  (dolist (repo (magit-list-repos))
    (progn
      (magit-status-setup-buffer repo)
      (unless (magit-anything-modified-p t repo)
        (magit-git-command-topdir "gh repo sync")
        (call-interactively 'magit-fetch-from-upstream)
        (call-interactively 'magit-section-show-level-4-all)))))


;; https://www.reddit.com/r/emacs/comments/rtvpvw/switching_to_a_buffer_of_the_same_mode/hqwvc9l/
(defun my-change-buffer-same-major-mode (change-buffer)
  "Call CHANGE-BUFFER until the current buffer has the initial `major-mode'."
  (let ((initial (current-buffer))
        (mode major-mode))
    (funcall change-buffer)
    (let ((first-change (current-buffer)))
      (catch 'loop
        (while (not (eq major-mode mode))
          (funcall change-buffer)
          (when (eq (current-buffer) first-change)
            (switch-to-buffer initial)
            (throw 'loop t)))))))

(defun my-next-buffer-same-major-mode ()
  "Like `next-buffer' for buffers in the current `major-mode'."
  (interactive)
  (my-change-buffer-same-major-mode 'next-buffer))
(global-set-key (read-kbd-macro "C-M-<right>") 'my-next-buffer-same-major-mode)

(defun my-previous-buffer-same-major-mode ()
  "Like `previous-buffer' for buffers in the current `major-mode'."
  (interactive)
  (my-change-buffer-same-major-mode 'previous-buffer))
(global-set-key
 (read-kbd-macro "C-M-<left>") 'my-previous-buffer-same-major-mode)

;; adapted from  a SO answer somewhere
(defun my-frame-move-resize (position)
  "Resize selected frame to cover exactly 1/3 of screen area. Move frame to
given third of current screen.  Symbol POSITION can be either left, center,
right."

  (let* ((HEIGHT (display-pixel-height))
         (WIDTH (display-pixel-width)))
    (pcase position
      ('left (setf x (* 0 (/ (display-pixel-width) 3))))
      ('center (setf x (* 1 (/ (display-pixel-width) 3))))
      ('right (setf x (* 2 (/ (display-pixel-width) 3)))))

    (set-frame-size (selected-frame) (round (/ WIDTH 1.5)) HEIGHT t)
    (set-frame-position (selected-frame) x 0)))

;; translated from something I have done on sway with i3ipc
(defun my-move-to-biggest (&optional arg)
  "A bit like crux transpose windows (where this function started from) but keep
   the largest window on focus.  If there is more than two window, focus to the
   largest window. if you are already on the largest window then switch to the
   last one (if prefix switch last one + prefix) Like this for i3/sway
   https://git.io/Jy94D or dwm centeredmaster
   https://dwm.suckless.org/patches/centeredmaster/"
  (interactive "p")
  (let ((this-win (selected-window))
        (this-buffer (window-buffer))
        (largest (get-largest-window))
        (this-win-size (* (window-total-width) (window-total-height))))
    (if (or (< (count-windows) 1) (eq this-win largest))
        (other-window arg)
      (select-window largest))
    (set-window-buffer this-win (current-buffer))
    (set-window-buffer (selected-window) this-buffer)
    (if (> this-win-size (* (window-total-width) (window-total-height)))
        (other-window -1))))

;; translated from something I have done on sway with i3ipc
(defun my-precious-layouts ()
  "if only one window split in two, two resize the one focused small and the
unfocused big, if 3 grow big the middle one, 1/6 of frame the side ones, if you
have more than 3 then well you better start using resize-window.el "
  (interactive)
  (if (eq (length (window-list)) 1)
      (split-window-right))
  (cond
   ((eq (length (window-list)) 2)
    ;; can't get window-resize work as expected so let shrink window minus the total frame width
    (shrink-window (- (window-total-width) (/ (frame-width) 4)) t))
   ((eq (length (window-list)) 3)
    (select-window (frame-first-window))
    (shrink-window (- (window-total-width) (/ (frame-width) 6)) t)
    (select-window (nth 2 (window-list nil nil (frame-first-window))))
    (shrink-window (- (window-total-width) (/ (frame-width) 6)) t)))
  (select-window (get-largest-window)))
(global-set-key (read-kbd-macro "C-c o") 'my-precious-layouts)

(defun my-window-split-toggle ()
  "Toggle between horizontal and vertical split with two windows."
  (interactive)
  (if (> (length (window-list)) 2)
      (error "Can't toggle with more than 2 windows!")
    (let ((func
           (if (window-full-height-p)
               #'split-window-vertically
             #'split-window-horizontally)))
      (delete-other-windows)
      (funcall func)
      (save-selected-window
        (other-window 1)
        (switch-to-buffer (other-buffer))))))
(global-set-key (read-kbd-macro "C-c <DEL>") 'my-window-split-toggle)

(defun my-switch-to-tab (n)
  `(lambda ()
     (interactive)
     (tab-bar-select-tab ,n)))

(defcustom my-scratches "~/Sync/scratches/"
  "Directory where to store scratch files."
  :type 'directory
  :group 'chmou)
(defun my-switch-scratch (type)
  (find-file
   (format "%s/%s.%s"
           (expand-file-name my-scratches)
           (format-time-string "%a-%d-%b-%Y")
           type)))

(defun my-focus-tab-or-run (name func)
  "Focus tab with name NAME or run FUNC if it doesn't exist."
  (let* ((recent-tabs
          (mapcar (lambda (tab) (alist-get 'name tab)) (tab-bar--tabs-recent))))
    (let ((tab-index (tab-bar--tab-index-by-name name)))
      (if tab-index
          (tab-bar-select-tab (1+ tab-index))
        (progn
          (tab-new)
          (funcall func))))))

(defvar my-vterm-command nil)
(defun my-vterm-execute-region-or-current-line (&optional arg)
  "Execute a command as ARG in vterm, pass variable or ask for it.

If ARG is non-nil, execute the current region in vterm, otherwise "
  (interactive "P")
  (require 'vterm)
  (eval-when-compile
    (require 'subr-x))
  (let ((command (or my-vterm-command (read-string "Enter a command: "))))
    (let ((buf (current-buffer))
          (vterm-buffer-name "*vterm*"))
      (unless (get-buffer vterm-buffer-name)
        (vterm))
      (display-buffer vterm-buffer-name t)
      (switch-to-buffer-other-window vterm-buffer-name)
      (vterm--goto-line -1)
      (vterm-send-string "")
      (vterm-send-string command)
      (vterm-send-return)
      (switch-to-buffer-other-window buf))))
(global-set-key (kbd "C-c 1") 'my-vterm-execute-region-or-current-line)

(defun my-github-search(&optional search)
  "Search current SEARCH with GitHub search."
  (interactive
   (list (if (use-region-p)
             (buffer-substring-no-properties (region-beginning) (region-end))
           (read-string "GitHub Search: " (thing-at-point 'symbol)))))
  (let* ((language-map (make-hash-table :test 'equal))
         (url nil))
    (mapc (lambda (pair) (puthash (car pair) (cdr pair) language-map))
          '(("c-mode"            . "C")
            ("python-mode"       . "Python")
            ("python-ts-mode"    . "Python")
            ("shell-script-mode" . "Shell")
            ("sh-mode"           . "Shell")
            ("typescript-mode"   . "TypeScript")
            ("javascript-mode"   . "JavaScript")
            ("ruby-mode"         . "Ruby")
            ("lua-mode"          . "Lua")
            ("rust-mode"         . "Rust")
            ("swift-mode"        . "Swift")
            ("clojurec-mode"     . "Clojure")
            ("lisp-mode"         . "Lisp")
            ("scheme-mode"       . "Scheme")
            ("racket-mode"       . "Scheme")
            ("go-ts-mode"        . "Go")
            ("go-mode"           . "Go")
            ("yaml-ts-mode"      . "YAML")
            ("yaml-mode" "       . YAML")
            ("json-ts-mode"      . "JSON")
            ("json-mode" "       . JSON")
            ("emacs-lisp-mode"   . "Emacs Lisp")))
    (setq url (format "https://github.com/search/?q=\"%s\"+language:\"%s\"&type=Code"
                      (url-hexify-string search)
                      (gethash (symbol-name major-mode) language-map "Text")))
    (browse-url url)))
(global-set-key (kbd "C-c /") 'my-github-search)
(provide 'init-functions)
