;; Yasnippet
(use-package yasnippet
  :general
  (general-vmap "SPC i" '(yas-insert-snippet :wk "Snippet insert"))
  (general-leader :states 'normal
    "i"   #'(yas-insert-snippet :wk "Snippet insert")
    "ly"  #'(:ignore t :wk "Yasnippet")
    "lyi" #'(yas-insert-snippet :wk "Insert snippet")
    "lyn" #'(yas-new-snippet :wk "New snippet")
    "lyv" #'(yas-visit-snippet-file :wk "Visit snippet files"))
  :bind
  ("C-S-y" . yas-insert-snippet)
  :hook
  (after-init . yas-global-mode)
  :custom
  (auto-insert-query nil)
  (yas-prompt-functions '(yas-completing-prompt))
  (yas-key-syntaxes '("w_" "w_." "^ "))
  (yas-expand-only-for-last-commands nil)
  :config
  (add-hook 'hippie-expand-try-functions-list 'yas-hippie-try-expand)
  (defun yas--magit-email-or-default ()
	"Get email from GIT or use default"
	(if (and (fboundp 'magit-toplevel)
             (magit-toplevel "."))
	    (magit-get "user.email")
	  user-mail-address)))

(use-package yasnippet-snippets :after yasnippet)

(provide 'init-yasnippet)
