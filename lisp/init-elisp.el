(use-package eldoc :defer t)

(use-package eldoc-eval
  :bind
  ([remap eval-expression] . eldoc-eval-expression))

(use-package aggressive-indent
  :hook
  (scheme-mode . aggressive-indent-mode)
  (lisp-mode . aggressive-indent-mode)
  (emacs-lisp-mode . aggressive-indent-mode))

(use-package elisp-autofmt
  :preface
  (setq
   elisp-autofmt-python-bin "python3"
   elisp-autofmt-cache-directory (locate-user-emacs-file "auto-save-list/elisp-autofmt-cache" ".elisp-autofmt-cache"))
  :commands (elisp-autofmt-mode elisp-autofmt-buffer)
  :general
  (general-leader emacs-lisp-mode-map :states 'normal "l f" '(elisp-autofmt-buffer :wk "Format Buffer")))

(use-package lisp
  :ensure nil
  :interpreter ("emacs" . emacs-lisp-mode)
  :custom (scheme-program-name "guile")
  :hook
  (emacs-lisp-mode . turn-on-eldoc-mode)
  (lisp-mode . turn-on-eldoc-mode)
  (lisp-interaction-mode . turn-on-eldoc-mode)
  :bind
  (:map emacs-lisp-mode-map
        ("C-c C-d" . edebug-defun)
        ("C-x C-e" . eval-buffer))
  :general
  (general-leader emacs-lisp-mode-map :states 'normal "l e" '(elisp-eval-region-or-buffer :wk "Eval Buffer")))

(use-package eros
  :commands (eros-eval-defun)
  :hook (emacs-lisp-mode . eros-mode))

(provide 'init-elisp)
