(use-package eglot-booster
  :vc (:url "https://github.com/jdtsmith/eglot-booster" :branch "main")
  :after eglot
  :config
  (eglot-booster-mode))

(use-package sideline-eglot)
(use-package sideline-flymake)
(use-package sideline
  :custom
  (sideline-delay 1.5)
  (sideline-eglot-code-actions-prefix "")
  (sideline-backends-right '(sideline-eglot sideline-flymake))
  :custom-face
  (sideline-default ((t (:foreground "gray50")))))

(use-package eglot
  :ensure nil
  :config
  (setq mode-line-misc-info
        (cl-remove-if (lambda (entry)
                        (eq (car entry) 'eglot--managed-mode))
                      mode-line-misc-info))

  :general
  (general-leader eglot-mode-map
    :states '(normal visual)
    "lq" #'eglot-reconnect
    "lb" #'consult-imenu
    "la" #'eglot-code-actions
    "l." #'eglot-code-action-quickfix
    "lf" #'eglot-format
    "le" #'flymake-show-project-diagnostics
    "li" #'eglot-find-implementation
    "lr" #'my-eglot-rename)
  (general-def eglot-mode-map
    :states '(normal visual)
    "g?" #'eldoc-doc-buffer
    "K"  #'eldoc-print-current-symbol-info
    "]e" #'flymake-goto-next-error
    "[e" #'flymake-goto-prev-error
    "gr" #'xref-find-references)
  :bind
  (:map eglot-mode-map
        ("C-S-i" . imenu)
        ("C-c a" . eglot-code-actions)
        ("C-c r" . eglot-reconnect)
        ("C-c [" . flymake-goto-prev-error)
        ("C-c ]" . flymake-goto-next-error))
  :config
  (setq-default
   eglot-workspace-configuration
   '((:gopls . ((usePlaceholders . t)
                (completeUnimported . t) ;;
                (staticcheck . t)
                (gofumpt . t)
                (directoryFilters . ["-vendor"])
                (buildFlags . ["-tags=e2e"])
                (gofumpt . t)))))
  (defun my-eglot-rename (newname)
    "Rename the current symbol to NEWNAME with initial input a."
    (interactive
     (list (read-from-minibuffer
            (format "Rename `%s' to: " (or (thing-at-point 'symbol t)
                                           (error "no symbol at point")))
            (or (thing-at-point 'symbol t) "") nil nil nil
            (symbol-name (symbol-at-point)))))
    (eglot--server-capable-or-lose :renameProvider)
    (eglot--apply-workspace-edit
     (jsonrpc-request (eglot--current-server-or-lose)
                      :textDocument/rename `(,@(eglot--TextDocumentPositionParams)
                                             :newName ,newname))
     current-prefix-arg))
  (fset #'jsonrpc--log-event #'ignore)
  (setopt eglot-events-buffer-size 0)
  (defun eglot-format-buffer-on-save ()
    (when (not (derived-mode-p 'python-ts-mode))
      (add-hook 'before-save-hook #'eglot-format-buffer -10 t)))
  (add-hook 'eglot-managed-mode-hook #'eglot-format-buffer-on-save)
  :hook
  (rust-mode . eglot-ensure)
  (rust-ts-mode . eglot-ensure)
  (sh-script-mode . eglot-ensure)
  (python-mode . eglot-ensure)
  (python-ts-mode . eglot-ensure)
  (json-mode . eglot-ensure)
  (json-ts-mode . eglot-ensure)
  (c-mode . eglot-ensure)
  (c-ts-mode . eglot-ensure)
  (cc-mode . eglot-ensure)
  (cc-ts-mode . eglot-ensure)
  (go-mode . eglot-ensure)
  (go-ts-mode . eglot-ensure)
  :custom
  rustic-lsp-client 'eglot)

(use-package eldoc-box
  :general
  (general-nmap prog-mode-map :states 'normal "K" #'eldoc-box-help-at-point)
  :custom
  (eldoc-box-clear-with-C-g t)
  (eldoc-box-max-pixel-width 1024))

(provide 'init-eglot)
