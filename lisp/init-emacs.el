;; Chmouel Boudjnah
;; most emacs stuff are going to be here
(use-package emacs
  :ensure nil
  :preface
  ;; FilePath
  (setq server-auth-dir (locate-user-emacs-file "auto-save-list/server/")
        url-configuration-directory (locate-user-emacs-file "auto-save-list/url/")
        url-cookie-file (locate-user-emacs-file "auto-save-list/cookies")
        tramp-persistency-file-name (locate-user-emacs-file "auto-save-list/tramp")
        gnus-init-file (locate-user-emacs-file "lisp/init-gnus.el")
        url-history-file (locate-user-emacs-file "auto-save-list/url-history")
        bookmark-default-file (locate-user-emacs-file "auto-save-list/bookmarks.bmk")
        save-place-file (locate-user-emacs-file "auto-save-list/emacs-places.el")
        nsm-settings-file (locate-user-emacs-file "auto-save-list/network-security.data"))
  :bind
  ("M-S-<up>"         . duplicate-dwim)
  ([remap upcase]     . upcase-dwim)
  ([remap downcase]   . downcase-dwim)
  ([remap capitalize] . capitalize-dwim)
  ([remap yank-pop]   . yank-from-kill-ring)
  ("C-<next>"         . end-of-buffer)
  ("s-q"              . kill-emacs)
  ("C-S-w"            . kill-current-buffer)
  ("C-M-d"            . copy-from-above-command)
  ("C-M-l"            . recenter-top-bottom)
  ("C-<prior>"        . beginning-of-buffer)
  ("C-c \]"           . next-error)
  ("C-c \["           . previous-error)
  ("C-x C-="          . ediff-buffers)
  ;; kill-current-buffer works weirdly for me, it switch to another buffer i am
  ;; not expected with and I have been using this keybining/lambda to a very long time
  ("C-x C-k"          . kill-current-buffer)
  ;; Kill the current buffer and delete its window, selecting the largest
  ;; remaining window.
  ("C-x K" . (lambda ()
               (interactive)
               (let ((buffer-to-kill (current-buffer))
                     (window-to-delete (selected-window)))
                 (kill-buffer buffer-to-kill)
                 (when (and (not (one-window-p))
                            (window-live-p window-to-delete))
                   (delete-window window-to-delete)
                   (select-window (get-largest-window))))))
  ("C-x f"            . find-file-at-point)
  ("M-q"              . fill-region)
  ("M-<SPC>"          . cycle-spacing)

  ("C-x <left>"       . indent-rigidly-left)
  ("C-x <right>"      . indent-rigidly-right)
  ("C-x C-z"          . suspend-frame)
  ("C-2"              . split-window-vertically)
  ("C-3"              . split-window-horizontally)

  ;; Redefine {for/back}ward paragraphs for 4 lines
  ("C-S-<down>" .
   (lambda ()
     (interactive)
     (ignore-errors (next-logical-line 5))
     (call-interactively 'evil-scroll-line-to-center)
     (pulse-momentary-highlight-one-line (point))))

  ("C-S-<up>" .
   (lambda ()
     (interactive)
     (ignore-errors (previous-logical-line 5))
     (call-interactively 'evil-scroll-line-to-center)
     (pulse-momentary-highlight-one-line (point))))
  ("C-x C" . kill-emacs)
  :hook
  (compilation-mode . (lambda () (setq truncate-lines nil)))
  (after-save . executable-make-buffer-file-executable-if-script-p)
  (after-init . global-auto-revert-mode)
  (after-init . winner-mode)
  (after-init . windmove-default-keybindings)
  (after-init . save-place-mode)
  (after-init . (lambda ()
                  (require 'server)
                  (unless (and (fboundp 'server-running-p) (server-running-p))
                    (server-start))))
  :config
  (add-function :after after-focus-change-function
                (lambda ()
                  (unless (frame-focus-state)
                    (save-some-buffers t))))
  (setq read-extended-command-predicate #'command-completion-default-include-p)
  (dolist (key '("\C-x\C-z" "\C-x\C-c" "\C-z" [f1] [f2] [f3] [f4] [f5] [f6] [f7] [f8] [f9] [f10] [f11] [f12]))
    (global-unset-key key))
  (defun my-select-buffer (window &rest _)
    (select-window window))

  (setq display-buffer-alist
        '(("\\*Warnings\\*"
           display-buffer-no-window
           (allow-no-window . t))
          ((or . ((derived-mode . Buffer-menu-mode)
                  (derived-mode . ibuffer-mode)
                  (derived-mode . compilation-mode)))
           (display-buffer-reuse-mode-window display-buffer-at-bottom)
           (body-function . my-select-buffer))
          ((or . ((derived-mode . flymake-diagnostics-buffer-mode)
                  (derived-mode . flymake-project-diagnostics-mode)
                  (derived-mode . messages-buffer-mode)
                  (derived-mode . rg-mode)
                  (derived-mode . occur-mode)
                  (derived-mode . ripgrep-mode)
                  (derived-mode . backtrace-mode)))
           (display-buffer-reuse-mode-window display-buffer-at-bottom)
           (body-function . my-select-buffer)
           (dedicated . t)
           (preserve-size . (t . t)))))
  :custom
  (truncate-string-ellipsis "…")
  (text-quoting-style 'curve)
  (split-width-threshold nil)
  (auto-revert-avoid-polling t)
  (completions-detailed t)
  (help-window-select t)
  (large-file-warning-threshold 20000000)
  (compilation-scroll-output t)
  (compilation-window-height 23)
  (compilation-max-output-line-length nil)
  (windmove-wrap-around t)
  (mouse-autoselect-window t)
  (initial-scratch-message nil)
  (kill-whole-line t)
  (kill-do-not-save-duplicates t)
  (use-short-answers t)
  (package-native-compile t)
  (tab-width 4)
  (indent-tabs-mode nil)
  (fill-column 80)
  ;; Backup
  (enable-local-variables :all) ;; Is it a good idea ?? probably not :\
  (version-control t)
  (backup-by-copying t)
  (delete-old-versions t)
  (kept-new-versions 6)
  (kept-old-versions 2)
  (create-lockfiles nil)
  (remote-file-name-inhibit-locks nil)
  ;; dont backup files opened by sudo
  (backup-enable-predicate
   (lambda (name)
     (and (normal-backup-enable-predicate name)
          (not
           (let ((method (file-remote-p name 'method)))
             (when (stringp method)
               (member method '("su" "sudo" "doas")))))))))

;; Diff
(use-package diff
  :bind
  (:map diff-mode-map
        ("K" . diff-hunk-previous)
        ("J" . diff-hunk-next)
        ("j" . next-line)
        ("k" . previous-line))
  :custom
  (diff-ignore-whitespace-hunk t)
  (diff-font-lock-prettify t)
  (diff-font-lock-syntax t))


;; XREF
(use-package xref
  :custom
  (xref-search-program 'ripgrep))

;; Ediff
(use-package ediff
  :custom
  (ediff-diff-options "-w")
  (ediff-split-window-function 'split-window-horizontally)
  (emerge-diff-options "--ignore-all-space")
  (ediff-window-setup-function 'ediff-setup-windows-plain))

;; Time
(use-package time
  :custom
  (display-time-24hr-format t)
  (display-time-default-load-average nil)
  (display-time-world-list
   '(("Europe/Paris" "Paris")
     ("America/New_York" "Boston")
     ("America/Los_Angeles" "San-Francisco")
     ("Asia/Kolkata" "Bangalore"))))

;; Save Places
(use-package saveplace
  :config
  (save-place-mode 1)
  :custom
  (save-place t))


(use-package ibuffer
  :bind
  ([remap list-buffers] . #'ibuffer)
  (:map ibuffer-mode-map
        ("k" . previous-line)
        ("j" . next-line))
  :custom
  (ibuffer-show-empty-filter-groups nil)
  (ibuffer-default-shrink-to-minimum-size t)
  (ibuffer-use-header-line nil)
  :hook
  (ibuffer-mode . hl-line-mode)
  :config
  (defun my-real-buffer-p (buf)
    (not (buffer-file-name buf))))

;; Hippy-Expand
(use-package hippie-exp
  :ensure nil
  :bind ([remap dabbrev-expand] . #'hippie-expand)
  :custom
  (hippie-expand-try-functions-list
   '(try-complete-file-name-partially
     try-complete-file-name
     try-expand-dabbrev
     try-expand-dabbrev-all-buffers
     try-expand-dabbrev-from-kill)))

(use-package tab-bar
  ;; :disabled
  :bind
  ;; ("C->" . tab-move)
  ;; ("C-<" .(lambda () (interactive)  (tab-move -1)))
  ("s-]" . tab-next)
  ("s-[" . tab-previous)
  :general
  (general-leader
    "jj" #'tab-recent
    "jt" :states 'normal '(tab-bar-mode :wk "toggle tab bar"))
  :custom
  (tab-bar-new-tab-choice 'dired-jump)
  ;; (tab-bar-show nil)
  (tab-bar-new-button-show 'nil)
  (tab-bar-auto-width nil)
  (tab-bar-close-button nil)
  (tab-bar-history-mode 'nil)
  (tab-bar-new-tab-to 'rightmost)
  (tab-bar-tab-hints nil)
  (tab-bar-separator "  ")
  :config
  (cond ((eq system-type 'darwin)
         (setq tab-bar-select-tab-modifiers '(super)))
        ((eq system-type 'gnu/linux)
         (setq tab-bar-select-tab-modifiers '(meta)))))

(use-package which-key
  :custom
  (which-key-separator " → " )
  :hook
  (after-init . which-key-mode)
  :config
  (which-key-setup-side-window-right-bottom))

(use-package tab-line
  :disabled
  :preface
  (defvar my-tab-line-ignore-buffer-regexps-exclude nil)
  (defvar my-tab-line-ignore-buffer-regexps-include nil)
  :defer nil
  :bind
  ("C-<tab>" . tab-line-switch-to-next-tab)
  ("C-S-<iso-lefttab>" . tab-line-switch-to-prev-tab)
  :hook
  (after-init . global-tab-line-mode)
  :custom
  (my-tab-line-ignore-buffer-regexps-exclude '("^\\*"))
  (my-tab-line-ignore-buffer-regexps-include
   (list (rx bol
             "*"
             (or "compilation"
                 "Go Test"
                 "scratch")
             "*" eol)))
  (tab-line-switch-cycling t)
  (tab-line-new-button-show nil)
  (tab-line-close-button-show nil)
  (tab-line-separator " | ")
  :config
  (setq tab-line-tabs-function #'tab-line-tabs-buffer-groups
        tab-line-tabs-buffer-group-function #'my-tab-line-tabs-buffer-group-by-project-real-file
        tab-line-tabs-buffer-list-function #'my-tab-line-tabs-buffer-list)
  (defun my-tab-line-tabs-buffer-group-by-project-real-file (&optional buffer)
    "Group tab buffers by project name, but only for real file buffers."
    (with-current-buffer (or buffer (current-buffer))
      (if (project-current)  ; Ensure the buffer is part of a project
          (concat "[" (project-name (project-current)) "]")
        "🚱")))
  (defun my-tab-line-tabs-buffer-list ()
    "Return a list of buffers to show in the tab line, excluding ignored buffers but including specified ones."
    (let ((buffers (append (list (current-buffer))
                           (mapcar #'car (window-prev-buffers))
                           (buffer-list))))
      (seq-filter (lambda (b)
                    (let ((buffer-name (buffer-name b)))
                      (and (buffer-live-p b)
                           (/= (aref buffer-name 0) ?\s) ; Exclude buffers starting with a space
                           (not (and (seq-some (lambda (regexp)
                                                 (string-match-p regexp buffer-name))
                                               my-tab-line-ignore-buffer-regexps-exclude)
                                     (not (seq-some (lambda (regexp)
                                                      (string-match-p regexp buffer-name))
                                                    my-tab-line-ignore-buffer-regexps-include)))))))
                  (delete-dups buffers)))))

(use-package minibuffer
  :ensure nil
  :bind (:map minibuffer-local-map
              ("C-." . next-history-element)))

(use-package pixel-scroll
  :ensure nil
  :preface
  (if (fboundp 'pixel-scroll-precision-mode)
      (pixel-scroll-precision-mode t))
  :init
  (defvar fk/default-scroll-lines 15)
  :custom
  (pixel-scroll-precision-interpolation-factor 1.0))

(use-package isearch
  :ensure nil
  :defer t
  :config
  ;; use selection to search
  ;; https://www.reddit.com/r/emacs/comments/2amn1v/isearch_selected_text/cixq7zx/
  (defun my-isearch-mode-default-string (orig-fun forward &optional regexp op-fun recursive-edit word-p)
    (if (and transient-mark-mode mark-active (not (eq (mark) (point))))
        (progn
          (isearch-update-ring (buffer-substring-no-properties (mark) (point)))
          (deactivate-mark)
          (funcall orig-fun forward regexp op-fun recursive-edit word-p)
          (if (not forward)
              (isearch-repeat-backward)
            (goto-char (mark))
            (isearch-repeat-forward)))
      (funcall orig-fun forward regexp op-fun recursive-edit word-p)))
  (advice-add 'isearch-mode :around #'my-isearch-mode-default-string)
  (defun my-occur-from-isearch ()
    "Invoke `consult-line' from ace-isearch."
    (interactive)
    (let ((query (if isearch-regexp
                     isearch-string
                   (regexp-quote isearch-string))))
      (isearch-update-ring isearch-string isearch-regexp)
      (let (search-nonincremental-instead)
        (ignore-errors (isearch-done t t)))
      (occur query)))
  :bind
  (:map isearch-mode-map
        ("C-d" . isearch-forward-symbol-at-point)
        ("C-o" . my-occur-from-isearch))
  :custom
  (isearch-allow-scroll t)
  (search-whitespace-regexp ".*"))

;; Recentfiles
(use-package recentf
  :ensure nil
  :hook (after-init . recentf-mode)
  :custom
  (recentf-max-saved-items 500)
  (recentf-exclude
   '("\\.?cache" ".cask" "url" "COMMIT_EDITMSG\\'" "bookmarks"
     "\\.\\(?:gz\\|gif\\|svg\\|elc\\|png\\|jpe?g\\|bmp\\|xpm\\)$"
     "\\.?ido\\.last$" "\\.revive$" "/G?TAGS$" "/.elfeed/"
     "^/tmp/" "^/var/folders/.+$" "^/ssh:" "/persp-confs/"
     (lambda (file) (file-in-directory-p file package-user-dir))))
  (recentf-save-file (locate-user-emacs-file "auto-save-list/recent-file-list.el"))
  (recentf-auto-cleanup "8:00am")
  (recentf-max-saved-items 500)
  :config
  (push (expand-file-name recentf-save-file) recentf-exclude)
  (add-to-list 'recentf-filename-handlers 'abbreviate-file-name))

;; Paren mode
(use-package paren
  :ensure nil
  :custom
  (show-paren-highlight-openparen t)
  (show-paren-when-point-in-periphery t)
  (show-paren-when-point-inside-paren t))

(use-package delsel
  :ensure nil
  :hook (after-init . delete-selection-mode))

(use-package calendar
  :ensure nil
  :custom
  (calendar-holidays `((holiday-fixed 1 1 "Jour de l'an")
                       (holiday-fixed 1 6 "Épiphanie")
                       (holiday-fixed 2 2 "Chandeleur")
                       (holiday-fixed 2 14 "Saint Valentin")
                       (holiday-fixed 5 1 "Fête du travail")
                       (holiday-fixed 5 8 "Commémoration de la capitulation de l'Allemagne en 1945")
                       (holiday-fixed 6 21 "Fête de la musique")
                       (holiday-fixed 7 14 "Fête nationale - Prise de la Bastille")
                       (holiday-fixed 8 15 "Assomption (Religieux)")
                       (holiday-fixed 11 11 "Armistice de 1918")
                       (holiday-fixed 11 1 "Toussaint")
                       (holiday-fixed 11 2 "Commémoration des fidèles défunts")
                       (holiday-fixed 12 25 "Noël")
                       ;; fetes a date variable
                       (holiday-easter-etc 0 "Pâques")
                       (holiday-easter-etc 1 "Lundi de Pâques")
                       (holiday-easter-etc 39 "Ascension")
                       (holiday-easter-etc 49 "Pentecôte")
                       (holiday-easter-etc -47 "Mardi gras")
                       (holiday-float 5 0 4 "Fête des mères")
                       ;; dernier dimanche de mai ou premier dimanche de juin si c'est le
                       ;; même jour que la pentecôte TODO
                       (holiday-float 6 0 3 "Fête des pères"))) ;; troisième dimanche de juin
  (calendar-mark-holidays-flag t)
  (calendar-week-start-day 1))

(use-package grep
  :ensure nil
  :hook
  (grep-mode . (lambda () (switch-to-buffer-other-window "*grep*")))
  :custom
  (grep-highlight-matches 'always))


; Scroll locking enablement
(use-package scroll-lock
  :ensure nil
  :commands
  (scroll-lock-previous-line scroll-lock-next-line))

(use-package ispell
  :ensure nil
  :custom
  (ispell-program-name "hunspell"))

(use-package flyspell
  :general
  (general-nmap "zg" :states 'normal '(my-flyspell-save-word :wk "flyspell-save-word"))
  :ensure nil
  :config
  ;; https://stackoverflow.com/questions/22107182/in-emacs-flyspell-mode-how-to-add-new-word-to-dictionary
  (defun my-flyspell-save-word ()
    (interactive)
    (let ((current-location (point))
          (word (flyspell-get-word)))
      (when (consp word)
        (flyspell-do-correct 'save nil (car word) current-location (cadr word) (caddr word) current-location))))
  (setq flyspell-prog-text-faces '(font-lock-comment-face font-lock-doc-face)))

(use-package grep
  :ensure nil
  :custom
  (grep-command "rg --vimgrep --no-heading --smart-case ")
  (grep-use-headings t))

(use-package ielm
  :ensure nil
  :hook
  (ielm-mode . turn-on-eldoc-mode)
  :custom
  (ielm-history-file-name (locate-user-emacs-file "auto-save-list/ielm-history.el")))

  (provide 'init-emacs)
;; init-emacs ends here
