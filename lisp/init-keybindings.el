(use-package general
  :config
  (general-evil-setup)

  ;; insert mode
  (general-imap
    "C-y"   #'yank
    "C-S-v" #'yank
    "RET"   #'default-indent-new-line
    "M-b"   #'backward-word
    "M-f"   #'forward-word
    "C-a"   #'crux-move-beginning-of-line
    "C-e"   #'end-of-line)

  ;; normal mode
  (general-nmap
    "]c"    #'evil-goto-last-change
    "[c"    #'evil-goto-last-change-reverse
    "[q"    #'previous-error
    "]q"    #'next-error
    "[m"    #'hydra-smerge/body
    "C-S-v" #'yank
    "C-e"   #'end-of-line
    "s"     #'evil-avy-goto-char-2
    "C-f"   #'evil-forward-char
    "C-b"   #'evil-backward-char
    "C-p"   #'evil-previous-line
    "C-n"   #'evil-next-line
    "C-r"   #'isearch-backward
    "U"     #'undo-redo
    "0"     #'crux-move-beginning-of-line
    "-"     #'er/expand-region)

  (general-create-definer general-leader
    :keymaps '(normal insert emacs)
    :non-normal-prefix "M-SPC"
    :prefix "SPC")

  (general-nmap
    :prefix "C-w"
    "F"   #'((lambda()(interactive)(my-frame-move-resize 'center)) :wk ("Center Frame"))
    "B"   #'my-move-to-biggest
    "u"   #'my-precious-layouts
    "i"   #'my-window-split-toggle
    "R"   #'resize-window)

  (general-nmap
    :prefix "g"
    "o"     #'ffap
    "a"     #'evil-lion-left
    "D"     #'xref-find-definitions-other-window
    "A"     #'evil-lion-right
    "x"     #'browse-url
    "-"     #'evil-numbers/dec-at-pt
    "="     #'evil-numbers/inc-at-pt)

  (general-leader
    '(normal visual)
    "]"   #'(tab-next  :wk "Tab Next" :repeat t)
    "["   #'(tab-previous  :wk "Tab Previous" :repeat t)
    "0"   #'(kill-buffer-and-window :wk "Kill buffer and window")
    "1"   #'((lambda() (interactive) (tab-bar-select-tab 1)) :wk "Select tab 1")
    "2"   #'((lambda() (interactive) (tab-bar-select-tab 2)) :wk "Select tab 2")
    "3"   #'((lambda() (interactive) (tab-bar-select-tab 3)) :wk "Select tab 3")
    "4"   #'((lambda() (interactive) (tab-bar-select-tab 4)) :wk "Select tab 4")
    "z"   #'(repeat :wk "Repeat")
    "u"   #'(universal-argument :wk "Universal argument")
    "e"   #'(dired-jump :wk "Dired")
    "x"   #'(execute-extended-command :wk "M-x")
    "C"   #'((lambda ()(interactive)(kill-current-buffer)(tab-close)) :wk "Kill buffer and tab")
    "c"   #'(kill-current-buffer :wk "Kill buffer")
    "`"   #'((lambda ()(interactive)(my-focus-tab-or-run "*vterm*" 'vterm)) :wk "Open Vterm")
    "g"   '(:ignore t :wk ("Git" . "Git prefix"))
    "f"   '(:ignore t :wk ("File" . "File prefix"))
    "ff"  #'(project-find-file :wk "Find in Project")
    "fc" #'((lambda()(interactive) (my-project-find-file-in-dir user-emacs-directory)) :wk "Find configuration file")
    "fq"  #'(read-only-mode :wk "Toggle Read Only")
    "fx"  '(:ignore t :wk ("Scratch" . "Scratch files prefix"))
    "fxx" #'((lambda()(interactive) (my-switch-scratch "txt")) :wk "Scratch Text ")
    "fxe" #'((lambda()(interactive) (my-switch-scratch "el")) :wk "Scratch Elisp ")
    "fxo" #'((lambda()(interactive) (my-switch-scratch "org")) :wk "Scratch Org ")
    "fxm" #'((lambda()(interactive) (my-switch-scratch "md")) :wk "Scratch Markdown")
    "fxp" #'((lambda()(interactive) (my-switch-scratch "py")) :wk "Scratch Python")
    "fr"  #'(consult-recent-file :wk "Recent File")
    "r"   #'(consult-recent-file :wk "Recent File")
    "q"   #'(:ignore t :wk "Quit")
    "qq"  #'(save-buffers-kill-terminal :wk "Quit Emacs")
    "qf"  #'(delete-frame :wk "Delete Frame")
    "qr"  #'(restart-emacs :wk "Restart Emacs")
    "qs"  #'(server-edit :wk "Server edit")
    "qR"  #'((lambda()(interactive)
               (desktop-save-mode -1)
               (save-buffers-kill-emacs t t))
             :wk "Restart without saving desktop")
    "SPC" #'((lambda()(interactive)
               (let ((consult-buffer-filter))
                 (add-to-list 'consult-buffer-filter "\\*")
                 (call-interactively 'consult-buffer))) :wk "Switch to Buffer")
    "/"   #'(consult-line :wk "Search for a line in buffer")
    "s"     '(:ignore t :wk "Search")
    "sn"  #'(evil-ex-nohighlight :wk "Clear highlight")
    ","   #'(vertico-repeat :wk "Repeat vectico search")
    "X"   #'(execute-extended-command-for-buffer :wk "M-x for buffer")
    ":"   #'(eval-expression :wk "Eval expression")
    "s"   #'(:ignore t :wk "Search")
    "ss"  #'((lambda () (interactive)
               (consult-ripgrep nil (thing-at-point 'symbol t)))
             :wk "Consult ripgrep project")
    "sG"  #'((lambda ()(interactive)
               (deadgrep
                (read-from-minibuffer "Search for text in this dir: "
                                      (thing-at-point 'symbol)) default-directory))
             :wk "Search in this directory")
    "sg"  #'(deadgrep :wk "Search in project")
    "sG"  #'((lambda ()(interactive)(rg-run (rg-read-pattern nil) "*" ".")) :wk "Search in Current")
    "sd"  #'((lambda ()(interactive)(consult-ripgrep ".")) :wk "Consult ripgrep current")
    "sh"  #'(Info-goto-emacs-command-node :wk "Search help")
    "sr"  #'(anzu-query-replace-at-cursor-thing :wk "Replace word at point")
    "sR"  #'(anzu-query-replace-regexp :wk "Replace Regexp")
    "sy"  #'(consult-yank-pop :wk "Clipboard history")
    "sb"  #'(consult-bookmark :wk "Switch to bookmark")
    "sa"  #'(my-github-search :wk "Search Github")
    "y"   #'(consult-yank-pop :wk "Clipboard history")
    "j"     '(:ignore t :wk "Jump to")
    "jn"  #'(bm-next :wk "Bookmark next" :repeat t)
    "jb"  #'(bm-toggle :wk "Bookmark toggle" :repeat t)
    "jN"  #'(bm-previous :wk "Bookmark previous" :repeat t)
    "jo"  #'(my-precious-layouts :wk "Layout Change" :repeat t)
    "p"     '(:ignore t :wk "Project")
    "pp"  #'(my-switch-to-project-dired :wk "Switch to Project")
    "ps"  #'(my-switch-to-project-search :wk "Grep in Project")
    "pf"  #'(project-find-file :wk "Find in Project")
    "pd"  #'(project-dired :wk "Dired in Project")
    "pc"  #'(project-compile :wk "Compile in Project")
    "pb"  #'(project-switch-to-buffer :wk "Switch to Project Buffer")
    "pk"  #'(project-kill-buffers :wk "Kill Project Buffers")
    "pr"  #'(my-find-recent-file-in-project :wk "List Recentf in Project")
    "b"   '(:ignore t :wk "Buffer")
    "bO"  #'(crux-kill-other-buffers :wk "Kill all other buffers")
    "bd"  #'(kill-current-buffer :wk "Kill buffer")
    "bb"  #'(consult-buffer :wk "Switch to buffer")
    "l"   #'(:ignore t :wk "LSP" :package lsp)
    "ld"  #'(flymake-show-buffer-diagnostics :wk "Show diagnostics")
    "t"     '(:ignore t :wk "Tabs and Toggle")
    "tn"  #'(tab-new :wk "New tab")
    "tL"  #'(toggle-truncate-lines :wk "Toggle truncate lines")
    "tv"  #'(visual-line-mode :wk "Toggle visual line mode")
    "tt"  #'(ff-find-other-file :wk "Find other File")
    "tl"  #'(display-line-numbers-mode :wk "Toggle line numbers")
    "tb"  #'(consult-project-buffer :wk "Switch to Buffer in new tab")
    "tB"  #'(bm-toggle :wk "Bookmark toggle" :repeat t)
    "tw"  #'(auto-fill-mode :wk "Toggle auto fill")
    "tf"  #'(find-file-other-tab :wk "Find file in another tab")
    "tr"  '(consult-recent-file :wk "Switch to Project in new tab")
    "tp"  '(my-switch-to-project-dired :wk "Switch to Project in new tab")
    "tk"  '(tab-close :wk "Tab Close"))

  (general-imap "j"
    (general-key-dispatch
        'self-insert-command :timeout 0.25 "j"
        (lambda()(interactive)
          (evil-normal-state)
          (cond ((eq major-mode 'vc-git-log-edit-mode)
                 (log-edit-done))
                ((memq 'gptel-mode local-minor-modes)
                 (gptel-send))
                ((string= (file-name-base (buffer-file-name)) "COMMIT_EDITMSG")
                 (call-interactively 'with-editor-finish))
                (t (save-buffer))))))
  (general-nmap emacs-lisp-mode-map  "K"   'helpful-at-point)
  (general-def  ibuffer-mode-map       "SPC" (general-key "SPC" :state 'normal))
  (general-def  dired-mode-map       "SPC" (general-key "SPC" :state 'normal))
  (general-def  Info-mode-map       "SPC" (general-key "SPC" :state 'normal))
  (general-def  view-mode-map        "SPC" (general-key "SPC" :state 'normal))
  (general-def  magit-status-mode-map
    "SPC" (general-key "SPC" :state 'normal)
    "C-;" #'magit-diff-show-or-scroll-up)

  (general-def
    :states   'motion
    :keymaps  '(helpful-mode-map help-mode-map)
    "[TAB]"   #'forward-button
    "[C-TAB]" #'backward-button
    "n"       #'forward-button
    "p"       #'backward-button
    "N"       #'evil-search-next)

  (general-def
    :states 'motion
    :keymaps 'go-test-mode-map
    "n" #'next-error-no-select
    "<tab>" #'next-error-no-select
    "g" #'recompile
    "N" #'evil-search-next)

  (general-def
    :states 'emacs
    :keymaps 'Info-mode-map
    "C-d" #'Info-scroll-up
    "C-u" #'Info-scroll-down
    "j" #'next-line
    "k" #'previous-line)

  (general-def
    :states 'emacs
    :keymaps 'xref--xref-buffer-mode-map
    "j" #'xref-next-line
    "k" #'xref-prev-line)

  (general-def
    :keymaps 'view-mode-map
    "q" #'kill-current-buffer)

  (general-def
    :keymaps 'rg-mode-map
    "k" #'previous-error-no-select
    "j" #'next-error-no-select))


(provide 'init-keybindings)
