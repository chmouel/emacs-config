;;; I use lazyvim as well and it has already everything i want
(defconst my-treesit-extra-path (locate-user-emacs-file "auto-save-list/tree-sitter"))
(defconst my-lazyvim-tressit-path (expand-file-name "~/.local/share/lazyvim/lazy/nvim-treesitter/parser"))
(when (file-exists-p my-lazyvim-tressit-path)
  (make-directory my-treesit-extra-path t)
  (let ((default-directory my-treesit-extra-path))
    ;; Remove broken symlinks
    (dolist (file (directory-files default-directory t "^[^.].*"))
      (when (and (file-symlink-p file) (not (file-exists-p (file-truename file))))
        (delete-file file)))
    ;; Create new symlinks
    (dolist (file (directory-files my-lazyvim-tressit-path t "^[^.].*"))
      (let ((link-name (concat "libtree-sitter-" (file-name-nondirectory file))))
        (unless (file-exists-p link-name)
          (make-symbolic-link file link-name t))))))

(setopt treesit-extra-load-path (list my-treesit-extra-path))
(use-package treesit-auto
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(use-package treesit
  :general
  (general-leader go-ts-mode-map :states 'normal "lw" 'my-copy-function-name-with-ts)
  :ensure nil
  :custom-face
  (font-lock-variable-use-face ((t (:inherit font-lock-punctuation-face))))
  :config
  (dolist (mapping
           '((python-mode . python-ts-mode)
             (css-mode . css-ts-mode)
             (typescript-mode . typescript-ts-mode)
             (js2-mode . js-ts-mode)
             (bash-mode . bash-ts-mode)
             (conf-toml-mode . toml-ts-mode)
             (go-mode . go-ts-mode)
             (css-mode . css-ts-mode)
             (json-mode . json-ts-mode)
             (js-json-mode . json-ts-mode)))
    (add-to-list 'major-mode-remap-alist mapping))
  (defun my-copy-function-name-with-ts()
    (interactive)
    (let ((funcname
           (substring-no-properties
            (treesit-node-text
             (treesit-node-child-by-field-name (treesit-defun-at-point) "name")))))
      (kill-new funcname)
      (message "Copied name: %s" funcname)))
  :custom
  (go-ts-mode-indent-offset 4)
  (treesit-font-lock-level 4))

(use-package expreg
  :bind
  ("C--" . expreg-contract)
  ("C-=" . expreg-expand))

(provide 'init-treesitter)
